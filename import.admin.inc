<?php

function import_export__import__import_via_file_form() {
  import_export_check_for_services_module(); // @@RECENT-CHANGE
  
  import_export__data_store__clear();
  
  $form = array();
  $form['#attributes'] = array('enctype' => 'multipart/form-data');

  $form['import_file'] = array(
    '#type' => 'file',
    '#title' => 'Export package',
    '#description' => 'Select the export package to import.',
  );

  $form['import_via_file'] = array(
    '#type' => 'submit',
    '#value' => 'Import from file',
  );

  return $form;
}

// admin/content/import-export/import/server
function import_export__list_export_servers_form() {
  import_export__data_store__clear();
  
  drupal_add_js(drupal_get_path('module', 'import_export') . '/includes/js/servers-form.js');
  drupal_add_css(drupal_get_path('module', 'import_export') . '/includes/css/import-servers-form.css');
  $form = array('#tree' => TRUE);
  
  $form['servers'] = array(
    '#type' => 'fieldset',
    '#title' => 'Saved Export Servers',
    '#description' => 'Use this list to save Export Servers that you use often.',
  );
  
  $form['servers']['servers-wrapper'] = array(
    '#prefix' => '<ol id="servers">',
    '#suffix' => '</ol>',
  );
  
  $servers = variable_get('import_export_remote_servers', array());
  foreach ($servers as $i => $server) {
    $form['servers']['servers-wrapper'][$i] = array(
      '#type' => 'markup',
      '#value' => "<input id=\"server-$i\" value=\"{$server['base url']}\" />
                    <span>
                      {$server['base url']}
                      <a href=\"/admin/content/import-export/import/server/list-packages/?url={$server['base url']}\" class=\"import-link\" id=\"import-server-$i\">import</a>
                      <a href=\"javascript:void(0);\" class=\"edit-link\" id=\"edit-server-$i\">edit</a>
                      <a href=\"javascript:void(0);\" class=\"delete-link\" id=\"delete-server-$i\">delete</a>
                    </span>",
      '#prefix' => '<li class="existing-server" id="li-server-'.$i.'"><div class="form-item">',
      '#suffix' => '</div></li>',
    );
  }
  
  $form['servers']['servers-wrapper']['new'] = array(
    '#type' => 'markup',
    '#value' => "<input class=\"new-server form-text\" id=\"new-server-1\" />",
    '#prefix' => '<li><div class="form-item">',
    '#suffix' => '</div></li>',
  );
  
  $form['add-server'] = array(
    '#type' => 'markup',
    '#value' => '<input type="button" value="Add another server" id="add-server" class="form-text"/>',
    '#prefix' => '<div class="form-item">',
    '#suffix' => '</div>',
  );
  
  $form['servers-data'] = array(
    '#type' => 'hidden',
    '#default_value' => '',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save server settings',
  );
  
  return $form;
}



function import_export__list_export_servers_form_submit($form, &$form_state) {
  $servers = explode('#', $form_state['values']['servers-data']);
  $to_save = array();
  foreach ($servers as &$server) {
    $server = urldecode($server);
    if (strlen(trim($server)) == 0) { // don't worry about blank fields
      continue;
    }
    if (!valid_url($server, TRUE)) {
      drupal_set_message("\"$server\" does not appear to be a valid URL and was not saved.", 'error');
    }
    else {
      $to_save[] = array('base url' => $server);
    }
  }
  variable_set('import_export_remote_servers', $to_save);
  drupal_set_message('Export Server URLs have been saved.');
  drupal_goto('admin/content/import-export/import/server');
}


// admin/content/import-export/import/server/list-packages?url=xxxx
function import_export__import__select_remote_profile_form() {
  import_export__data_store__clear();
  module_load_include('inc', 'import_export', 'classes/import_export_profile');
  module_load_include('inc', 'import_export', 'import_export.messages');
  module_load_include('inc', 'import_export', 'import_export.services');
  
  if (!isset($_GET['url'])) {
    drupal_set_message('Must specify server in "url" GET variable.', 'error');
    drupal_goto('admin/content/import-export/import/server');
  }
  
  $server_base_url = import_export_ensure_export_server_base_url($_GET['url']);
  $profiles = import_export_use_service($server_base_url, 'import_export');
  import_export_show_any_messages_in_data($profiles);
  
  if (!$profiles && !is_array($profiles)) {
    drupal_set_message('Error retrieving profiles from selected Export Server.', 'error');
    drupal_goto('admin/content/import-export/import/server');
  }
  else if (is_array($profiles) && count($profiles) == 0) {
    drupal_set_message('No profiles are available on the specified Export Server.', 'error');
    drupal_goto('admin/content/import-export/import/server');
  }
  
  foreach ((array)$profiles as $epid => $profile) {
    $options[$epid] = "Package $epid: " . $profile->description;
  }
  $epids = array_keys($options);
  $default = array_shift($epids);

  $form = array();
  $form['base_url'] = array(
    '#type' => 'value',
    '#value' => $server_base_url,
  );
  $form['profiles'] = array(
    '#type' => 'radios',
    '#title' => 'The following packages were found at the selected Export Server',
    '#description' => 'Choose a package to import',
    '#options' => $options,
    '#default_value' => $default,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Import selected package',
  );
  return $form;
}



function import_export__import__select_remote_profile_form_submit($form, &$form_state) {
  import_export__data_store__clear();
  module_load_include('inc', 'import_export', 'import_export.messages');
  
  $epid = $form_state['values']['profiles'];
  $server_base_url = $form_state['values']['base_url'];

  $package = import_export_use_service($server_base_url, "import_export/$epid");
  import_export_show_any_messages_in_data($package);

  if ($package) {
    $metadata = array('epid' => $epid, 'method' => 'server', 'base url' => $server_base_url);
    $store = array('package' => $package, 'metadata' => $metadata);
    $success = import_export__data_store__store(NULL, $store);
    drupal_goto('admin/content/import-export/import/confirm');
  }
  else {
    drupal_set_message(t("Error contacting remote server while attempting to contact service at URL '{@base_url}@service': '{@error}'",
                        array('@base_url' => $server_base_url, '@service' => "import_export/$epid.php", '@error' => $response->error)), 'error');
    drupal_goto('admin/content/import-export/import/server');
  }
}



function import_export__import__import_via_file_form_submit($form, &$form_state) {
  import_export__data_store__clear();
  
  module_load_include('inc', 'import_export', 'classes/import_export_package');
  if ($f = file_save_upload('import_file')) {
    
    $store = array(
              'metadata' => array(
                  'method' => 'file',
                  'uploaded file object' => $f
                )
              );
    
    $success = import_export__data_store__store(NULL, $store);
    drupal_goto('admin/content/import-export/import/confirm');
  }
  else {
    drupal_set_message('Error saving uploaded import file.  Check permissions and temporary files folder and try again.', 'error');
    drupal_goto('admin/content/import-export/import');
  }
}

function import_export_import_settings_form(&$form_state) {
  import_export_load_unserialize_dependencies();
  $plugins = import_export_get_data_plugin_info_all('import');
  $form = array('#tree' => TRUE);
  $metadata = import_export__data_store__retrieve('metadata');
  
  $form['check_all'] = array(
    '#type' => 'checkbox',
    '#title' => 'Select all items',
    '#description' => '',
  );
  
  if ($package = import_export__data_store__retrieve_sparse_package($metadata['method'])) {
    // get form elements from all plugins and wrap them in fieldsets
    foreach ($plugins as $name => $plugin) {
      if (import_export_plugins_are_enabled($name) == FALSE) {
        continue;
      }
      $plugin = import_export_plugin($name);
      $form[$plugin->my_key()] = $plugin->import_settings_form($form_state, $form, $package);
      $form[$plugin->my_key()]['#type'] = 'fieldset';
      $form[$plugin->my_key()]['#title'] = import_export_get_plugin_info($plugin->my_name(), 'title');
      $form[$plugin->my_key()]['#collapsible'] = TRUE;
    }

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Import selected data',
    );
    
    $form['#redirect'] = 'admin/content/import-export/import';
    
    drupal_add_css(drupal_get_path('module', 'import_export') . '/includes/css/import-settings-form.css');
    return $form;
  }
}




function import_export_import_settings_form_submit($form, &$form_state) {
  $metadata = import_export__data_store__retrieve('metadata');
  
  // process the import settings form into a list of IDs to import and other settings
  $import_settings = import_export_plugin_invoke_all_by_ref('process_import_settings_form', 'import', $form_state, $form);
  module_load_include('inc', 'import_export', 'import_export.batch');
  import_export__import__do_batch($import_settings);
}




