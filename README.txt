When transforming a profile into a package, you must take care of preparing the export ids yourself:

$profile = import_export_profile($epid)
$ids = {all plugins}::prepare_export_ids($profile)
new import_export_package($ids, {sparse?})
      (automatically called {all plugins}::export_objects, {all plugins}::import_export_package_alter)
      
When you do this, the following plugin hooks are called:

::prepare_export_ids
  ::should_export_object (each object's settings)
  ::valid_id (each object's id)
::export_objects
  ::get_exportable_object (each object)
  
These objects are stored in $package->data:
  $package->data[$plugin_key] = array($object_id => $object, $object_id => $object, ...)
... where $plugin_key is the key for each plugin and $object_id is either a string or an integer, depending on what kind of unique key each plugin has to deal with in the database.