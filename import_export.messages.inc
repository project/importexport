<?php

function import_export_show_any_messages_in_data(&$data) {
  if (is_array($data) and isset($data['__import_export_messages'])) {
    foreach ((array)$data['__import_export_messages'] as $message) {
      if (isset($message['message'], $message['vars']) && is_array($message['vars'])) {
        drupal_set_message(t($message['message'], $message['vars']));
      }
      else if (isset($message['message'])) {
        drupal_set_message(t($message['message']));
      }
    }
    unset($data['__import_export_messages']);
  }
  else if (is_object($data) and isset($data->__import_export_messages)) {
    foreach ((array)$data->__import_export_messages as $message) {
      if (isset($message['message'], $message['vars']) && is_array($message['vars'])) {
        drupal_set_message(t($message['message'], $message['vars']));
      }
      else if (isset($message['message'])) {
        drupal_set_message(t($message['message']));
      }
    }
    unset($data->__import_export_messages);
  }
}

function import_export_add_message($message, $vars, $status = 'status') {
  $messages = &ctools_static('import_export : messages', array());
  $messages[] = array('message' => $message, 'vars' => $vars, 'status' => $status);
}

function import_export_get_messages() {
  $messages = ctools_static('import_export : messages', array());
  return $messages;
}

function import_export_attach_messages(&$data) {
  $messages = import_export_get_messages();
  if (count($messages) > 0) {
    if (is_object($data)) {
      $data->__import_export_messages = $messages;
    }
    else if (is_array($data)) {
      $data['__import_export_messages'] = $messages;
    }
  }
}

/*function import_export_show_all_messages($clear = TRUE) {
  $messages = ctools_static('import_export : messages', array());
  foreach ($messages as $message) {
    drupal_set_message(t($message['message'], $message['vars']), $message['status']);
  }
  if ($clear) {
    import_export_clear_messages();
  }
}*/

/*function import_export_clear_messages() {
  $messages = &ctools_static('import_export : messages', array());
  $messages = array();
}*/

/*function import_export_add_any_messages_in_data($data) {
  if (is_array($data) and isset($data['__import_export_messages'])) {
    foreach ((array)$data['__import_export_messages'] as $message) {
      import_export_add_message($message['message'], $message['vars'], $message['status'])
    }
  }
}*/
