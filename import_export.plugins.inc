<?php

/**
 * Defining a data plugin:
 * 
 * $plugins['menu'] = array(  // key this by the plugin name
 *   'title' => 'Menus',      // Used administratively
 *     'handler' => array(
 *       'path' => drupal_get_path('module', 'import_export') .'/plugins',    // path to the plugin file
 *       'file' => 'import_export_menu_data.inc',                             // the filename of the plugin file
 *       'class' => 'import_export_menu_data',  // the name of the plugin class
 *       'parent' => 'import_export_data_type', // should generally always be import_export_data_type
 *     ),
 *     'table info' => array(
 *       'id field' => 'menu_name',     // the unique primary key of the main table that this plugin works with.  this ensures that id translations happen automatically and correctly.
 *       'id field type' => 'varchar',  // the database type of the primary key.  used to create placeholders in queries, etc.
 *       'table' => 'menu_custom',      // the name of the database table.  will be automatically prefixed.
 *     ),
 *     'key' => 'menu',                 // the key -- this is used when assembling forms, import_export_packages, import_export_profiles, etc. -- it can be altered by other modules, so always call $plugin->my_key() to get it.  the name and the key are generally the same.
 *     'module dependencies' => array('menu'),    // modules that this plugin depends on.  the plugin will be unavailable unless these modules are enabled.
 *     'import dependencies' => array( 
 *       'node', 'site',                          // when importing, this is the list of plugins that need to import their data before this plugin can perform its import.
 *     ),
 *     'export dependencies' => array( 
 *       'menu',                                  // when exporting, this is the list of plugins that need to prepare their export data before this plugin can prepare its export.
 *     ),
 *     'unserialize dependencies' => array( 
 *       drupal_get_path('module', 'mymodule') . '/includes/helperClass.inc', // list of files that should be loaded before unserializing data added by this plugin
 *     ),
 *   );
 */

/**
 * Default Import/Export plugins.
 */
function _import_export_default_plugins() {
  $plugins = array();

  /**
   * Conditions.
   */
  $plugins['import_export_data_interface'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'import_export') .'/plugins',
      'file' => 'import_export_data_interface.inc',
      'class' => 'import_export_data_interface',
    ),
  );
  $plugins['import_export_data_type'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'import_export') .'/plugins',
      'file' => 'import_export_data_type.inc',
      'class' => 'import_export_data_type',
      'parent' => 'import_export_data_interface',
    ),
  );
  $plugins['__nothing'] = array(
    'title' => 'Nothing',
    'handler' => array(
      'path' => drupal_get_path('module', 'import_export') .'/plugins',
      'file' => 'import_export_data_type_nothing.inc',
      'class' => 'import_export_data_type_nothing',
      'parent' => 'import_export_data_type',
    ),
  );
  $plugins['menu'] = array(
    'title' => 'Menus',
    'handler' => array(
      'path' => drupal_get_path('module', 'import_export') .'/plugins',
      'file' => 'import_export_menu_data.inc',
      'class' => 'import_export_menu_data',
      'parent' => 'import_export_data_type',
    ),
    'table info' => array(
      'id field' => 'menu_name',
      'id field type' => 'varchar',
      'table' => 'menu_custom',
    ),
    'key' => 'menu',
    'module dependencies' => array('menu'),
    'import dependencies' => array('node'),
  );
  $plugins['node'] = array(
    'title' => 'Nodes',
    'handler' => array(
      'path' => drupal_get_path('module', 'import_export') .'/plugins',
      'class' => 'import_export_node_data',
      'parent' => 'import_export_data_type',
      'file' => 'import_export_node_data.inc',
    ),
    'table info' => array(
      'id field' => 'nid',
      'id field type' => 'int',
      'table' => 'node',
    ),
    'key' => 'node',
    'module dependencies' => array('node', 'node_export'),
    'import dependencies' => array('site', 'file', /*'term'*/), // @@TODO: reenable term
    'export dependencies' => array('site', 'menu'),
  );
  $plugins['site'] = array(
    'title' => 'Sites',
    'handler' => array(
      'path' => drupal_get_path('module', 'import_export') .'/plugins',
      'file' => 'import_export_site_data.inc',
      'class' => 'import_export_site_data',
      'parent' => 'import_export_data_type',
    ),
    'table info' => array(
      'id field' => 'sid',
      'id field type' => 'int',
      'table' => 'sites',
    ),
    'key' => 'site',
    'module dependencies' => array('sites'),
    'import dependencies' => array(),
  );
  $plugins['file'] = array(
    'title' => 'Files',
    'handler' => array(
      'path' => drupal_get_path('module', 'import_export') .'/plugins',
      'file' => 'import_export_file_data.inc',
      'class' => 'import_export_file_data',
      'parent' => 'import_export_data_type',
    ),
    'table info' => array(
      'id field' => 'fid',
      'id field type' => 'int',
      'table' => 'files',
    ),
    'key' => 'file',
    'module dependencies' => array('node_export_file'),
    'import dependencies' => array(),
    'export dependencies' => array('node'),
  );
  /*$plugins['term'] = array(
    'title' => 'Taxonomy terms',
    'handler' => array(
      'path' => drupal_get_path('module', 'import_export') .'/plugins',
      'file' => 'import_export_term_data.inc',
      'class' => 'import_export_term_data',
      'parent' => 'import_export_data_type',
    ),
    'table info' => array(
      'id field' => 'tid',
      'id field type' => 'int',
      'table' => 'term_data',
    ),
    'key' => 'term',
    'module dependencies' => array('taxonomy'),
    'import dependencies' => array('vocabulary'),
    'export dependencies' => array(),
  );*/
  
  return $plugins;
}