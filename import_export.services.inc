<?php

/**
 * Implementation of hook_services_resources().
 */
function import_export_services_resources() {
  return array(
    // @@RECENT-CHANGE
    /*'import_export_nodes' => array(
      'actions' => array(
        'download' => array(
          'help' => 'Retrieve an export package of nodes',
          'file' => array('type' => 'inc', 'module' => 'import_export', 'name' => 'import_export.services'),
          'callback' => '_import_export_service__nodes__retrieve',
          'access callback' => '_import_export_service_access',
          'access arguments' => array('nodes', 'retrieve'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'settings',
              'type' => 'array',
              'description' => 'An array of node IDs keyed to export settings for each node',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ),
      ),
    ),
    'import_export_sites' => array(
      'actions' => array(
        'download' => array(
          'help' => 'Retrieve an export package of sites',
          'file' => array('type' => 'inc', 'module' => 'import_export', 'name' => 'import_export.services'),
          'callback' => '_import_export_service__sites__retrieve',
          'access callback' => '_import_export_service_access',
          'access arguments' => array('sites', 'retrieve'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'settings',
              'type' => 'array',
              'description' => 'An array of site IDs keyed to export settings for each site',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ),
      ),
    ),
    'import_export_menus' => array(
      'actions' => array(
        'download' => array(
          'help' => 'Retrieve an export package of menus',
          'file' => array('type' => 'inc', 'module' => 'import_export', 'name' => 'import_export.services'),
          'callback' => '_import_export_service__menus__retrieve',
          'access callback' => '_import_export_service_access',
          'access arguments' => array('menus', 'retrieve'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'settings',
              'type' => 'array',
              'description' => 'An array of menu_names keyed to export settings for each menu',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ),
      ),
    ),
    'import_export_files' => array(
      'actions' => array(
        'download' => array(
          'help' => 'Retrieve an export package of files',
          'file' => array('type' => 'inc', 'module' => 'import_export', 'name' => 'import_export.services'),
          'callback' => '_import_export_service__files__retrieve',
          'access callback' => '_import_export_service_access',
          'access arguments' => array('files', 'retrieve'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'settings',
              'type' => 'array',
              'description' => 'An array of file IDs keyed to export settings for each file',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ),
      ),
    ),*/
    
    // @@RECENT-CHANGE
    /*'import_export_package' => array(
      'actions' => array(
        'download' => array(
          'help' => 'Retrieve an export package based on the passed array of settings',
          'file' => array('type' => 'inc', 'module' => 'import_export', 'name' => 'import_export.services'),
          'callback' => '_import_export_service__package__download',
          'access callback' => '_import_export_service_access',
          'access arguments' => array('package', 'download'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'settings',
              'type' => 'array',
              'description' => 'An array of plugin names keyed to arrays of object IDs keyed to export settings for each object',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ),
      ),
    ),*/
    
    
   'import_export' => array(
     'retrieve' => array(
        'help' => 'Retrieve an export package',
        'file' => array('type' => 'inc', 'module' => 'import_export', 'name' => 'import_export.services'),
        // @@RECENT-CHANGE
        'callback' => '_import_export_service_retrieve',
        //'callback' => '_import_export_service__package__download',
        'access callback' => '_import_export_service_access',
        'access arguments' => array('retrieve'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'id',
            'type' => 'int',
            'description' => 'The id of the package to get',
            'source' => array('path' => '0'),
            'optional' => FALSE,
          ),
        ),
      ),
     'index' => array(
       'help' => 'Retrieves a listing of packages',
       'file' => array('type' => 'inc', 'module' => 'import_export', 'name' => 'import_export.services'),
       'callback' => '_import_export_service_package_index',
       'access callback' => '_import_export_service_access',
       'access arguments' => array('index'),
       'access arguments append' => FALSE,
       'args' => array(),
     ),
     
     'actions' => array(
       // @@RECENT-CHANGE
       /*'download' => array(
        'help' => 'Download specified components of an export package',
        'file' => array('type' => 'inc', 'module' => 'import_export', 'name' => 'import_export.services'),
        'callback' => '_import_export_service_package_download',
        'access callback' => '_import_export_service_access',
        'access arguments' => array('download'),
        'access arguments append' => FALSE,
        'args' => array(
          array(
            'name' => 'id', // @@TODO remove id, perhaps refactor this entire resource
            'type' => 'int',
            'description' => 'The id of the package to get',
            'source' => array('path' => '0'),
            'optional' => FALSE,
            ),
          array(
            'name' => 'settings',
            'type' => 'array',
            'description' => 'Array of modifications to default export package',
            'source' => 'data',
            'optional' => TRUE,
            ),
          ),
        ), // end download
        */
        'download' => array(
          'help' => 'Retrieve an export package based on the passed array of settings',
          'file' => array('type' => 'inc', 'module' => 'import_export', 'name' => 'import_export.services'),
          'callback' => '_import_export_service__package__download',
          'access callback' => '_import_export_service_access',
          'access arguments' => array('package', 'download'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'settings',
              'type' => 'array',
              'description' => 'An array of plugin names keyed to arrays of object IDs keyed to export settings for each object',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ), // end download
        
      ), // end actions
    ), // end import_export
  );
}



// @@RECENT-CHANGE
function import_export_default_services_endpoint() {
  $endpoints = array();
  $endpoint = new stdClass;
  $endpoint->api_version = 3;
  $endpoint->debug = 0;
  $endpoint->status = 1;
  $endpoint->name = 'import_export';
  $endpoint->title = 'Import/Export module Export Server';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'export-server';
  $endpoint->authentication = array();
  $endpoint->resources = array(
    'import_export' => array(
      'alias' => '',
      'operations' => array(
        'retrieve' => array(
          'enabled' => 1,
        ),
        'index' => array(
          'enabled' => 1,
        ),
      ),
      'targeted actions' => array(
        'download' => array(
          'enabled' => 1,
        ),
      ),
      'actions' => array(
        'download' => array(
          'enabled' => 1,
        ),
      ),
    ),
  );
  $endpoints[] = $endpoint;
  return $endpoints;
}



function _import_export_service_access($op) {
  //$args = func_get_args();print_r($args);exit;
  // @@TODO: validate SESSID
/*  global $user;
  if ($user->uid == 0) {
    return FALSE;
  }
*/  
  return TRUE;
}



// retrieve a sparse package based on a profile
function _import_export_service_retrieve($id) {
  module_load_include('inc', 'import_export', 'classes/import_export_profile');
  module_load_include('inc', 'import_export', 'classes/import_export_package');
  module_load_include('inc', 'import_export', 'import_export.core');
  module_load_include('inc', 'import_export', 'import_export.messages');
  $profile = new import_export_profile($id);
  
  if ($profile) {
    $ids = array();
    import_export_plugin_invoke_all_by_ref('prepare_export_ids', 'export', $ids, $profile);
    $package = new import_export_package($ids, TRUE); // create a sparse package
    import_export_attach_messages($package);
    return $package;
  }
  return 'blablabla'; // @@TODO: error handling?
}



function _import_export_service__package__download($settings) {
  module_load_include('inc', 'import_export', 'classes/import_export_package');
  module_load_include('inc', 'import_export', 'import_export.messages');
  
  $ids = array();
  import_export_plugin_invoke_all_by_ref('prepare_export_ids', 'export', $ids, $settings);
  $package = new import_export_package($ids);
  import_export_attach_messages($package);
  return $package;
}



/*function _import_export_service__nodes__retrieve($settings) {
  if (import_export_plugins_are_enabled('node') == FALSE) { // @@TODO: move this to the access function maybe
    return;
  }
  module_load_include('inc', 'import_export', 'classes/import_export_package');
  $ids = import_export_plugin_invoke_all('prepare_export_ids', 'export', array(import_export_plugin('node')->my_key() => $settings));
  $package = new import_export_package($ids);
  return $package;
}

function _import_export_service__sites__retrieve($settings) {
  return import_export_plugin('site')->export_objects(array($key => $settings));
}

function _import_export_service__menus__retrieve($settings) {
  return import_export_plugin('menu')->export_objects(array($key => $settings));
}

function _import_export_service__files__retrieve($settings) {
  return import_export_plugin('file')->export_objects(array($key => $settings));
}*/


function _import_export_service_package_index() {
  module_load_include('inc', 'import_export', 'classes/import_export_profile');
  module_load_include('inc', 'import_export', 'import_export.messages');
  
  $profiles = array();
  $res = db_query("SELECT epid FROM {import_export_profiles}");
  while ($epid = db_result($res)) {
    $profiles[$epid] = $epid;
  }
  foreach ($profiles as $epid => $v) {
    $profiles[$epid] = new import_export_profile($epid);
  }
  import_export_attach_messages($profiles);
  return $profiles;
}


/* @@RECENT-CHANGE
function _import_export_service_package_download($export_settings = NULL) {
  module_load_include('inc', 'import_export', 'import_export.messages');
  
  $package = new import_export_package($export_settings);
  import_export_attach_messages($package);
  return $package;
}*/



// (@@TODO: remove if possible) this fixes some weird form-related bug in services
function import_export_services_resource_settings_alter(&$res_set, $resource) {
  alter_recurse($res_set);
}
function alter_recurse(&$item) {
  if (is_array($item)) {
    $item_copy = (array)$item;
    foreach ($item as $k => $v) {
      if ($k == 'services_oauth') {
        unset ($item[$k]);
      }
      else {
        alter_recurse($item[$k]);
      }
    }
  }
}



function import_export_remote_service_login($server_base_url, $username, $password) {
  module_load_include('inc', 'import_export', 'import_export.messages');
  // Login
  $user_data = array(
    'name' => $username,
    'pass' => $password,
  );
  $response = import_export_use_service($server_base_url, 'user/login', 'php', 'POST', NULL, $user_data);
  if (!$response) {
    return FALSE;
  }
  import_export_set_export_server_session_data($server_base_url, $response);
  import_export_show_any_messages_in_data($response);
}



function import_export_get_export_server_session_data($server_base_url) {
  $session_data = import_export__data_store__retrieve('export server sessions');
  if (!is_array($session_data)) {
    $session_data = array();
  }
  return first_existing($session_data[$server_base_url], NULL);
}



function import_export_set_export_server_session_data($server_base_url, $data) {
  $session_data = import_export__data_store__retrieve('export server sessions');
  if (!is_array($session_data)) {
    $session_data = array();
  }
  $session_data[$server_base_url] = (array)$data;
  import_export__data_store__store('export server sessions', $session_data);
}



function import_export_ensure_export_server_base_url($server_base_url) {
  if (strrchr($server_base_url, '/') !== '/') { // if url doesn't end with a trailing slash, add one
    $server_base_url .= '/';
  }
  // @@TODO: make this prefix/endpoint name variable somehow?
  if (strpos($server_base_url, 'export-server') === FALSE) { // export-server endpoint prefix isn't present
    $server_base_url .= 'export-server/';
  }
  return $server_base_url;
}



function import_export_use_service($server_base_url, $resource, $response_format = 'php', $method = 'GET', $headers = array(), $data = NULL) {
  if (module_exists('rest_server')) {
    //$resource = str_replace('/index', '', $resource); // explicitly specifying "/index" is unnecessary and incorrect when using the REST server
  }
  if (!$response_format) {
    $response_format = 'php';
  }
  if (!$headers) {
    $headers = array();
  }
  if ($method == 'POST') {
    $headers['Content-Type'] = 'application/x-www-form-urlencoded';
  }
  if ($data) {
    $data = http_build_query($data, '', '&');
  }
  
  //if (($session_data = import_export_get_export_server_session_data($server_base_url))) {
    //$headers['Cookie'] = $session_data['session_name'] . '=' . $session_data['sessid'];
  //}

  $server_base_url = import_export_ensure_export_server_base_url($server_base_url);
  $resource_url = $server_base_url . "$resource.$response_format";
  $response = drupal_http_request($resource_url, $headers, $method, $data);
  if ($response->error) {
    import_export_add_message("import_export_use_service(): Error contacting remote server while attempting to contact service at URL '{@base_url}{@resource}.@format': '{@error}'",
                              array('@base_url' => $server_base_url, '@resource' => $resource, '@format' => $response_format, '@error' => $response->error), 'error');
    watchdog('import_export', "import_export_use_service(): Error contacting remote server while attempting to contact service at URL '{$server_base_url}{$resource}.$response_format': '{$response->error}'", WATCHDOG_ERROR);
    return FALSE;
  }
    
  $decode_func = '';
  switch($response_format) {
    case 'php':
      $decode_func = 'unserialize';
      import_export_load_unserialize_dependencies();
      break;
    case 'json':
      $decode_func = 'json_decode';
      break;
  }
  
  if (function_exists($decode_func)) {
    return $decode_func($response->data);
  }
  else {
    return FALSE;
  }
}



