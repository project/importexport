<?php

function import_export_profile_list() {
  import_export_check_for_services_module(); // @@RECENT-CHANGE
  
  $profiles = import_export_get_profiles();
  $rows = array();
  foreach ($profiles as $profile) {
    $edit_link = l('edit basic settings', 'admin/content/import-export/profile/' . $profile->epid . '/edit');
    $edit_advanced_link = l('edit advanced settings', 'admin/content/import-export/profile/' . $profile->epid . '/edit/advanced');
    $download_link = l('download export', 'admin/content/import-export/profile/' . $profile->epid . '/download');
    $delete_link = l('delete', 'admin/content/import-export/profile/' . $profile->epid . '/delete');
    $rows[] = array($profile->epid, $profile->description, implode('<br>', array($edit_link, $edit_advanced_link, $download_link)));
  }
  $output = theme('table', array(t('ID'), t('Description'), 'Actions'), $rows);
  $output .= drupal_get_form('import_export_settings_form');
  return $output;
}

function import_export_profile_delete_confirm_form($form_state, $profile) {
  $form = array();
  $form['profile'] = array(
    '#type' => 'value',
    '#value' => $profile
  );
  $form['#redirect'] = 'admin/content/import-export/profile';
  $form = confirm_form($form, 'Are you sure you want to delete this profile?', 'admin/content/import-export/profile');
  return $form;
}

function import_export_profile_delete_confirm_form_submit($form, &$form_state) {
  $epid = $form_state['values']['profile']->epid;
  import_export_profile::delete($epid);
  drupal_set_message("Profile $epid has been deleted.");
}

function import_export_settings_form() {
  import_export_check_for_services_module(); // @@RECENT-CHANGE
  $form = array();
  
  $form['header'] = array(
    '#value' => '<h3>Import Export Settings</h3>',
  );

  $form['import_export_namespace'] = array(
    '#type' => 'textfield',
    '#title' => 'Namespace',
    '#description' => 'Make sure the namespace on each of your Drupal sites is different.  When objects are being exported from one site to
                      another, Import Export uses namespaces to keep track of where those objects came from.  This way, they can be updated
                      correctly during later imports.  The default namespace for a site is simply its HTTP host through which it\'s being
                      accessed.  However, if a site can be accessed through multiple URLs (as with the Sites or Domain modules, for instance),
                      you should consider setting your own namespace to avoid confusion.',
    '#default_value' => import_export_get_namespace(),
  );
  
  if (import_export_plugins_are_enabled('node')) {
    $form['import_export_stop_all_node_export_resets'] = array(
      '#type' => 'checkbox',
      '#title' => 'Stop all Node Export resets',
      '#description' => 'By default, Node Export resets a number of fields on a node when that node is imported:
                        the publishing options, created time, changed time, path, book settings, and menu position.  
                        Checking this box will ensure that none of these resets occur.  If you wish to have more
                        finely grained control over the resets Node Export performs, leave this box unchecked and
                        visit the ' . l('Node Export settings page', 'admin/settings/node_export') . '.',
      '#default_value' => variable_get('import_export_stop_all_node_export_resets', FALSE),
    );
    
    if (import_export_plugins_are_enabled('file')) {
      $form['import_export_get_imagebrowser_files_from_nodes'] = array(
        '#type' => 'checkbox',
        '#title' => 'Automatically export ImageBrowser files found in exported nodes',
        '#default_value' => variable_get('import_export_get_imagebrowser_files_from_nodes', TRUE),
      );
    }
  }

  return system_settings_form($form);
}

function import_export_profile_basic_form(&$form_state, $profile = NULL) {
  import_export_check_for_services_module(); // @@RECENT-CHANGE
  
  $form = array('#tree' => TRUE);

  $form['epid'] = array(
    '#type' => 'value',
    '#value' => ($profile ? $profile->epid : 0),
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => 'Description of this export profile (for admin reference)',
    '#description' => '',
    '#default_value' => ($profile ? $profile->description : ''),
    '#weight' => -100,
  );
  
  // get fields from all plugins
  $form += import_export_plugin_invoke_all_by_ref('profile_basic_settings_form', 'export', $form_state, $form, $profile);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => ($profile ? 'Save export profile' : 'Create export profile'),
    '#weight' => 100,
  );

  return $form;
}

function import_export_profile_basic_form_submit($form, &$form_state) {
  module_load_include('inc', 'import_export', 'classes/import_export_profile');
  $profile = new import_export_profile;

  if ((int)$form_state['values']['epid'] > 0) {
    $old_profile = new import_export_profile((int)$form_state['values']['epid']);
    $profile->epid = $form_state['values']['epid'];
  }
  $profile->description = $form_state['values']['description'];

  // let each plugin process the form and add data to the profile
  $profile->data = import_export_plugin_invoke_all_by_ref('process_profile_basic_settings_form', 'export', $form_state, $form, $old_profile);

  if (!$profile->save() or !$profile->epid) {
    drupal_set_message('Error saving export profile.', 'error');
  }
  
  if ($old_profile) { // editing existing profile
    $form_state['redirect'] = 'admin/content/import-export/profile';
  }
  else { // new profile -- send user to advanced edit form
    $form_state['redirect'] = 'admin/content/import-export/profile/' . $profile->epid . '/edit/advanced';
  }
}

function import_export_profile_advanced_form(&$form_state, $profile) {
  import_export_check_for_services_module(); // @@RECENT-CHANGE
  
  if (!$profile) {
    return FALSE;
  }

  $form = array('#tree' => TRUE);

  $form['epid'] = array(
    '#type' => 'value',
    '#value' => $profile->epid,
  );

  // get fields from all plugins
  $form += import_export_plugin_invoke_all_by_ref('profile_advanced_settings_form', 'export', $form_state, $form, $profile);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save export profile',
  );
  return $form;
}

function import_export_profile_advanced_form_submit($form, &$form_state) {
  $profile = new import_export_profile((int)$form_state['values']['epid']);
  $old_profile = (array)$profile; // copy

  // let each plugin process the form and add data to the profile
  $profile->data = import_export_plugin_invoke_all_by_ref('process_profile_advanced_settings_form', 'export', $form_state, $form, $old_profile);

  if (!$profile->save()) {
    drupal_set_message('Error saving export profile.', 'error');
  }
  $form_state['redirect'] = 'admin/content/import-export/profile';
}

/* @@TODO: decide if this is wanted or not
$form['menus']['node_behavior_menu__' . $menu_name] = array(
  '#type' => 'radios',
  '#title' => 'Nodes in menu "' . $menu_name . '"',
  '#options' => array(t('Create all new nodes on the receiving site'),
                      t('Update nodes that have the same nids, create new nodes when no matching nid is found'),
                      t('Update only nodes that have matching nids, create no new nodes'),
                      t('Do nothing with these nodes')),
  '#default_value' => $menu['node behavior'],
);
*/

function import_export_download_export_code($profile) {
  if (!$profile || !$profile->epid) {
    return FALSE;
  }
  module_load_include('inc', 'import_export', 'classes/import_export_package');
  
  $ids = array();
  import_export_plugin_invoke_all_by_ref('prepare_export_ids', 'export', $ids, $profile);
  $package = new import_export_package($ids);
  $contents = $package->get_export_code();
  $filepath = file_directory_temp() . '/import-export-' . md5(time()) . '.export';

  if ($fp = fopen($filepath, 'w')) {
    $success = fwrite($fp, $contents);
    fclose($fp);
    if ($success) {
      import_export_push_file_download($filepath);
    }
    else { // if the temporary directory isn't writeable, just echo the import code to the browser
      drupal_set_message('Problem writing to temporary directory.', 'error');
      echo $contents;
      exit;
    }
  }
  else { // if the temporary directory isn't writeable, just echo the import code to the browser
    drupal_set_message('Problem writing to temporary directory.', 'error');
    echo $contents;
    exit;
  }
}

/**
 * This is what adds the headers which activates the force downloading.
 */
function import_export_push_file_download($filepath) {
	$filepath = file_create_path($filepath);
	header('Content-Type: text/plain');
	header('Content-Disposition: attachment; filename="' . basename($filepath) . '";');
		// Content-Length is also a good header to send, as it allows the browser to
		// display a progress bar correctly.
		// There's a trick for determining the file size for files over 2 GB. Nobody
		// should be using this module with files that large, but… the sprintf()
		// trickery makes sure the value is correct for files larger than 2GB. See
		// note at http://php.net/filesize
	header('Content-Length: ' . sprintf('%u', filesize($filepath)));
	readfile($filepath);
	exit;
}
