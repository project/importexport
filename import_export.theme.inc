<?php

// this is used by the import_export_node_data plugin to theme its section of the import confirm form

// theme the list of nodes to be imported into a table
function theme_import_export_import_settings_form_node_table($element) {
  $header = array();
  $rows = array();
  $sites_enabled = import_export_plugins_are_enabled('site');
  
  foreach (element_children($element) as $key) {
    $controls = drupal_render($element[$key]['import']) . '<br>' . drupal_render($element[$key]['import_behavior']);
    
    if ($sites_enabled) {
      $rows[] = array(
                  drupal_render($element[$key]['nid']),
                  drupal_render($element[$key]['title']),
                  drupal_render($element[$key]['sites']),
                  drupal_render($element[$key]['exists']),
                  $controls);
    }
    else {
      $rows[] = array(
                  drupal_render($element[$key]['nid']),
                  drupal_render($element[$key]['title']),
                  drupal_render($element[$key]['exists']),
                  $controls);
    }
  }
  
  if ($sites_enabled) {
    return theme('table', array(t('NID'), t('Title'), t('Sites'), t('Exists on this server?'), t('Options')), $rows);
  }
  else {
    return theme('table', array(t('NID'), t('Title'), t('Exists on this server?'), t('Options')), $rows);
  }
}