<?php

class import_export_data_type_nothing extends import_export_data_type {
  public function my_name() {}
  public function id_field() {}
  public function my_key() {}
  public function export_package_alter(&$import_export_package, $export_base, $sparse = FALSE) {}
  public function should_export_object($settings) {}
  public function should_import_object($settings) {}
  public function valid_id($id) {}
  public function process_profile_advanced_settings_form(&$form_state, $form, $old_profile) {}
  public function get_exportable_object($id, $sparse = FALSE) {}
  public function process_profile_basic_settings_form(&$form_state, $form, $old_profile = NULL) {}
  public function export_objects($export_settings, $sparse = FALSE) {}  
  public function prepare_import_package(&$import_export_package, $method, $import_settings, $import_data) {}
  public function import_settings_form(&$form_state, $form, $import_export_package) {}
  public function process_import_settings_form(&$form_state, $form) {}
  
  public function import($import_export_package, $import_settings) {
    return FALSE;
  }
  
  public function profile_basic_settings_form(&$form_state, $form, $profile = NULL) {
    return array();
  }
  
  public function profile_advanced_settings_form(&$form_state, $form, $profile) {
    return array();
  }
  
  public function prepare_export_ids(&$ids, $export_base) {
    return array();
  }
  
  
}