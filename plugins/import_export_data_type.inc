<?php

abstract class import_export_data_type implements import_export_data_interface {
  public function id_field() {
    return import_export_get_plugin_info($this->my_name(), 'table info', 'id field');
  }
  
  public function my_key() {
    return import_export_get_plugin_info($this->my_name(), 'key');
  }
  
  public function should_export_object($settings) {
    if (!isset($settings['import']) || $settings['import'] != FALSE) {
      return TRUE;
    }
    return FALSE;
  }
  
  public function should_import_object($settings) {
    if (!isset($settings['import']) || $settings['import'] != FALSE) {
      return TRUE;
    }
    return FALSE;
  }
  
  public function valid_id($id) {
    $info = import_export_get_plugin_info($this->my_name(), 'table info');
    $res = db_query("SELECT COUNT(*) FROM {{$info['table']}} WHERE {$info['id field']} = " . db_placeholders(array($id), $info['id field type']), $id);
    $row = db_result($res);
    if ($row > 0) {
      return TRUE;
    }
    return FALSE;
  }
  
  public function prepare_export_ids(&$ids, $export_base) {
    if ($export_base instanceof import_export_profile) { // import_export_profile
      $data = $export_base->data;
    }
    else if (is_array($export_base)) { // settings array
      $data = $export_base;
    }
    else {
      return array();
    }

    foreach ((array)$ids[$this->my_key()] as $id => $settings) {
      if (!isset($data[$this->my_key()][$id])) {
        $data[$this->my_key()][$id] = $settings;
      }
    }

    foreach ((array)$data[$this->my_key()] as $id => $settings) {
      if ($this->should_export_object($settings) && $this->valid_id($id)) {
        if (!isset($ids[$this->my_key()])) {
          $ids[$this->my_key()] = array();
        }
        $ids[$this->my_key()][$id] = $settings;
      }
      else {
        unset($ids[$this->my_key()][$id]);
      }
    }
    //return $ids;
  }
  
  public function export_objects($export_settings, $sparse = FALSE) {
    $export = array();

    foreach ($export_settings[$this->my_key()] as $id => $settings) {
      $obj = $this->get_exportable_object($id, $sparse);
      if ($obj) {
        $obj->__import_export_metadata = first_existing($obj->__import_export_metadata, array());
        $obj->__import_export_metadata = array_merge_recursive($obj->__import_export_metadata, $export_settings[$this->my_key()][$id]); // add all settings fields to object
        $obj->__import_export_metadata[$this->id_field()] = $id; // ensure that the id field is present and correct
        $export[$id] = $obj;
      }
      else { // @@TODO: should log somewhere too
        import_export_add_message('import_export_data_type::export_objects() -- Invalid id specified (id: @id, class: @class).', array('@id' => $id, '@class' => $this->my_name()), 'error');
        /*drupal_set_message(t('import_export_data_type::export_objects() -- Invalid id specified (id: @id, class: @class).',
                            array('@id' => $id, '@class' => $this->my_name())), 'error');*/
      }
    }
    return $export;
  }
  
  public function export_package_alter(&$import_export_package, $export_base, $sparse = FALSE) {
  }
  
  public function prepare_import_package(&$import_export_package, $method, $import_settings, $import_data) {
    foreach ($import_settings[$this->my_key()] as $id => $settings) {
      // remove unwanted objects (this is done by the remote server, so this is only really going to be useful during a file import)
      if (!$this->should_import_object($settings)) {
        $import_export_package->remove($this->my_key(), $id);
      }
    }
  }
  
  public function import_settings_form(&$form_state, $form, $import_export_package) {
    $elements = array();

    foreach ($import_export_package->get($this->my_key()) as $id => $object) {
      $origin_id = $object->__import_export_metadata[$this->id_field()];
      
      $elements[$origin_id]['#prefix'] = '<div class="import-item item-' . $this->my_key() . '">';
      $elements[$origin_id]['#suffix'] = '</div>';
      
      $elements[$origin_id]['import'] = array(
        '#type' => 'checkbox',
        '#title' => 'Import',
        '#default_value' => TRUE,
      );
      
      if ($new_id = import_export_translate_id($import_export_package->origin_namespace, $this->id_field(), $origin_id)) {
        $elements[$origin_id]['import_behavior'] = array(
          '#type' => 'radios',
          '#title' => 'Import behavior',
          '#options' => array(t('Update existing (id in this database: @new_id)', array('@new_id' => $new_id)), t('Create new')),
          '#default_value' => ($object->__import_export_metadata['update_existing'] ? IMPORT_UPDATE_EXISTING : IMPORT_CREATE_NEW),
        );
      }
    }
    return $elements;
  }
  
  // should return an array of the structure (id => settings, id => settings, ...)
  // if you structure your part of the import settings form cleverly, you won't need to implement this function
  public function process_import_settings_form(&$form_state, $form) {
    return $form_state['values'][$this->my_key()];
  }
}


