<?php

class import_export_file_data extends import_export_data_type {

  public function my_name() {
    return 'file';
  }

  public function get_exportable_object($id, $sparse = NULL) {
    module_load_include('inc', 'import_export', 'import_export.messages');
    
    $res = db_query("SELECT * FROM {files} WHERE fid = %d", $id);
    $file = db_fetch_object($res);
    if (!$file) {
      import_export_add_message('Invalid file ID specified: @fid', array('@fid' => $fid), 'error');
      return FALSE;
    }
    if (!isset($file->filepath) || !is_file($file->filepath)) {
      import_export_add_message("File not found: '!path'", array('!path' => $file->filepath), 'error');
      return FALSE;
    }

    $export_mode = variable_get('node_export_file_mode', 'inline');
    $file->__import_export_metadata['export mode'] = $export_mode;

    if ((in_array($export_mode, array('local', 'remote')) || !$sparse) && _node_export_file_check_assets_path($export_mode, $assets_path) !== FALSE) {
      $assets_path = variable_get('node_export_file_assets_path', '');
      $export_mode = variable_get('node_export_file_mode', 'inline');
      $file->contents = _node_export_file_get_export_data($file, $export_mode, $assets_path);
    }
    _node_export_file_clean_local_file_path($file->filepath);
    return $file;
  }

  public function valid_id($id) {
    if (parent::valid_id($id)) { // basic check to make sure the id is in my table
      if ($filepath = $this->_get_filepath_by_fid($id) and is_file(dirname($_SERVER['SCRIPT_FILENAME']) . '/' . $filepath)) { // check for existence of the file
        return TRUE;
      }
    }
    return FALSE;
  }

  public function prepare_export_ids(&$ids, $export_base) {
    // @@TODO: grep for full filenames in node->body, lookup fid, see if fid is already going to be exported, if not, add it
    // @@TODO: grep in cck fields, etc
    if ($export_base instanceof import_export_profile) { // import_export_profile
      $data = $export_base->data;
    }
    else if (is_array($export_base)) { // settings array
      $data = $export_base;
    }
    else {
      return array();
    }

    // export files embedded as imagebrowser tokens in node->body fields
    if (import_export_plugins_are_enabled('node') && variable_get('import_export_get_imagebrowser_files_from_nodes', TRUE)) {
      $node_key = import_export_plugin('node')->my_key();
      $nids = array_keys($ids[$node_key]);
      foreach ((array)$nids as $nid) {
        $node = node_load((int)$nid);
        if ($node) {
          $matches = array();
          if ($count = preg_match_all('/\[ibimage==(?P<fid>\d+)/', $node->body, $matches)) {
            foreach ((array)$matches['fid'] as $fid) {
              $ids[$this->my_key()][$fid] = array(
                                              $this->my_key() => $fid,
                                              'from imagebrowser token' => TRUE,
                                              'from nid' => $nid);
            }
          }
        }
      }
    }

    // @@TODO: try to remove this and just mark files as needing to be regenerated
    // search for original/thumbnail/preview versions of this file and add them too (these are created by image.module)
//    $data_copy = (array)$data[$this->my_key()];
    foreach ((array)$data[$this->my_key()] as $fid => $settings) {
      $file = $this->_get_file_by_fid($fid);
      if (in_array($file->filename, array('thumbnail', 'preview', '_original'))) {
        $res = db_query("SELECT i2.*, f2.* FROM {files} f
                          JOIN {image} i ON f.fid = i.fid
                          JOIN {image} i2 ON i.nid = i2.nid
                          JOIN {files} f2 ON f2.fid = i2.fid
                          WHERE f.fid = %d AND i2.fid != f.fid", $fid);

        while ($row = db_fetch_object($res)) {
          if (is_file($row->filepath)) {
            $ids[$this->my_key()][$row->fid] = array(
                $this->my_key() => $row->fid,
                'from image module' => TRUE,
                'from nid' => $row->nid,
            );
          }
        }
      }
    }

    parent::prepare_export_ids($ids, $export_base); // run standard checks on all gathered objects (should_export and valid_id)
    //return $ids;
  }

  public function import($import_export_package, $import_settings) {
    module_load_include('inc', 'import_export', 'import_export.messages');
    
    // create all directories needed by files on nodes if node_export_file is going to try to import them
    if (import_export_plugins_are_enabled('node') && module_exists('node_export_file')) {
      $node_key = import_export_plugin('node')->my_key();
      $node_code = $import_export_package->other_luggage[$node_key]['node_code']; // @@TODO: some way to ask the plugin for this key?
      if ($node_code && is_array(eval('return ' . $node_code . ';'))) {
        $nodes = node_export_node_decode($node_code);
      }

      foreach ((array)$nodes as $node) { // attempt to create all necessary directories (recursively) to facilitate the import of the files on this node
        if (!$this->_ensure_all_file_paths($node)) {
          import_export_add_message('Failed to ensure directories for files on node (origin nid: @origin_nid)',
                                array('@origin_nid' => $node->__import_export_metadata[import_export_plugin('node')->id_field()]), 'error');
        }
      }
    }

    $file_data = $import_export_package->get($this->my_key());
    $all_success = TRUE;

    // process files
    foreach ((array)$file_data as $file) {
      $origin_fid = $file->__import_export_metadata[$this->id_field()];

      // put file data where node_export_file expects it to be on the $file object
      if ($file->contents) {
        switch ($file->__import_export_metadata['export mode']) {
          case 'local':
            $export_var = 'node_export_file_path'; break;
          case 'remote':
            $export_var = 'node_export_file_url';  break;
          default:
          case 'inline':
            $export_var = 'node_export_file_data'; break;
        }
        $file->$export_var = $file->contents;
      }

      // UPDATE EXISTING FILE
      $new_fid = (int)import_export_translate_id($import_export_package->origin_namespace, $this->id_field(), $origin_fid);
      if ($new_fid && $import_settings[$this->my_key()][$origin_fid]['import_behavior'] == IMPORT_UPDATE_EXISTING) {
        $file->fid = $new_fid;
        _node_export_file_unclean_local_file_path($file->filepath);
        if (!$this->_ensure_path($file->filepath)) {
          import_export_add_message('Error writing file to destination directory (origin fid: @origin_fid, filepath: @filepath).',
                              array('@origin_fid' => $origin_fid, '@filepath' => $file->filepath), 'error');
        }
        else {
          $this->_import_helper_update_existing($file); // update file
        }
      }

      // CREATE NEW FILE
      else {
        _node_export_file_unclean_local_file_path($file->filepath);
        if (!$this->_ensure_path($file->filepath)) {
          import_export_add_message('Error writing file to destination directory (origin fid: @origin_fid, filepath: @filepath).',
                              array('@origin_fid' => $origin_fid, '@filepath' => $file->filepath), 'error');
        }
        else {
          import_export_add_message('Success ensuring path: @filepath', array('@filepath' => $file->filepath), 'status'); // @@TODO: remove or make debug-only
        }

        $success = _node_export_file_import_file($file); // import file
        if ($success && $file->fid) {
          import_export_add_id_translation($import_export_package->origin_namespace, $this->id_field(), $origin_fid, $file->fid); // add id translation
        }
        else {
          $all_success = FALSE;
          import_export_add_message('Error importing new file (origin fid: @origin_fid, filepath: @filepath).',
                              array('@origin_fid' => $origin_fid, '@filepath' => $file->filepath), 'error');
        }
      }
    }
    return $all_success;
  }

  // this function is adapted from node_export's _node_export_file_import_file().
  // the only real change was to add the 'fid' key to all calls to drupal_write_record().
  private function _import_helper_update_existing(&$file) {
    module_load_include('inc', 'import_export', 'import_export.messages');
    
    _node_export_file_unclean_local_file_path($file->filepath);

    // The file is already in the right location AND either the
    // node_export_file_path is not set or the node_export_file_path and filepath
    // contain the same file
    // FIXME: same filesize does NOT mean "same file".
    if (is_file($file->filepath) &&
      (
        !is_file($file->node_export_file_path) ||
        (
          is_file($file->node_export_file_path) &&
          filesize($file->filepath) == filesize($file->node_export_file_path)
        )
      )
    ) {
      drupal_write_record('files', $file, array('fid'));
    }
    elseif (isset($file->node_export_file_data)) {
      $directory = file_create_path(dirname($file->filepath));

      if (file_check_directory($directory, TRUE)) {
        if (file_put_contents($file->filepath, base64_decode($file->node_export_file_data))) {
          drupal_write_record('files', $file, array('fid'));
        }
      }
    }
    // The file is in a local location, move it to the
    // destination then finish the save
    elseif (isset($file->node_export_file_path) && is_file($file->node_export_file_path)) {
      $directory = file_create_path(dirname($file->filepath));

      if (file_check_directory($directory, TRUE)) {
        // The $file->node_export_file_path is passed to reference, and modified
        // by file_copy().  Making a copy to avoid tainting the original.
        $node_export_file_path = $file->node_export_file_path;
        file_copy($node_export_file_path, $directory, FILE_EXISTS_REPLACE);

        // At this point the $file->node_export_file_path will contain the
        // destination of the copied file
        $file->filepath = $node_export_file_path;

        drupal_write_record('files', $file, array('fid'));
      }
    }
    // The file is in a remote location, attempt to download it
    elseif (isset($file->node_export_file_url)) {
      // Need time to do the download
      ini_set('max_execution_time', 900);

      $temp_path = file_directory_temp() . '/' . md5(mt_rand()) .'.txt';
      if (($source = fopen($file->node_export_file_url, 'r')) == FALSE) {
        import_export_add_message("Could not open '@file' for reading.", array('@file' => $file->node_export_file_url), 'error');
        return FALSE;
      }
      elseif (($dest = fopen($temp_path, 'w')) == FALSE) {
        import_export_add_message("Could not open '@file' for writing.", array('@file' => $file->filepath), 'error');
        return FALSE;
      }
      else {
        // PHP5 specific, downloads the file and does buffering
        // automatically.
        $bytes_read = @stream_copy_to_stream($source, $dest);

        // Flush all buffers and wipe the file statistics cache
        @fflush($source);
        @fflush($dest);
        clearstatcache();

        if ($bytes_read != filesize($temp_path)) {
          import_export_add_message("Remote export '!url' could not be fully downloaded, '@file' to temporary location '!temp'.", array('!url' => $file->node_export_file_url, '@file' => $file->filepath, '!temp' => $temp_path), 'error');
          return FALSE;
        }
        // File was downloaded successfully!
        else {
          if (!@copy($temp_path, $file->filepath)) {
            unlink($temp_path);
            import_export_add_message("Could not move temporary file '@temp' to '@file'.", array('@temp' => $temp_path, '@file' => $file->filepath), 'error');
            return FALSE;
          }

          unlink($temp_path);
          $file->filesize = filesize($file->filepath);
          $file->filemime = file_get_mimetype($file->filepath);
        }
      }

      fclose($source);
      fclose($dest);

      drupal_write_record('files', $file, array('fid'));
    }
    // Unknown error
    else {
      import_export_add_message("Unknown error occured attempting to import file.", NULL, 'error');
      return FALSE;
    }

    return TRUE;
  }

  public function profile_basic_settings_form(&$form_state, $form, $profile = NULL) {
    // should return form elements to be added
    $elements = array();
    $files = $this->_get_all_valid_files();
    $fids = array();
    $selected_files = array();

    foreach ((array)$profile->data[$this->my_key()] as $fid => $settings) {
      $fids[$fid] = $fid;
      $selected_files[$fid] = $files[$fid];
    }

    $elements['all_files'] = array(
      '#type' => 'select',
      '#title' => '',
      '#description' => '',
      '#options' => $files,
      '#prefix' => '<span style="display:none">',
      '#suffix' => '</span>',
    );

    $elements['files'] = array(
      '#type' => 'fieldset',
      '#title' => 'Files',
    );

    $elements['files']['file_autocomplete'] = array(
      '#type' => 'textfield',
      '#title' => 'Filter file names',
      '#description' => 'Begin typing the name of a file to filter down the list.',
    );

    $elements['files']['autocomplete_files'] = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => 'Available files (filtered)',
      '#size' => 4,
      '#description' => 'Select the files to export.',
      '#options' => $files,
    );

    $elements['files']['add_files'] = array(
      '#type' => 'markup',
      '#value' => '<input type="button" id="edit-add-files" value="Add selected files" class="form-submit" />',
    );

    $selected_files_markup = '';
    foreach ((array)$selected_files as $fid => $filepath) {
      $selected_files_markup .= '<option value="'.$fid.'">'.$filepath.' ('.$fid.')</option>';
    }
    $selected_files_string = implode(',', array_keys($selected_files));

    $elements['files']['selected_files'] = array(
      '#type' => 'markup',
      '#title' => 'Files to export',
      '#description' => 'Add files to export.',
      '#prefix' => '<div class="form-item" id="edit-selected-files-wrapper"><label for="edit-selected-files">Files to export: </label>',
      '#suffix' => '</div>',
      '#value' => '<select name="selected_files[]" multiple="multiple" class="form-select" id="edit-selected-files" size="5">'.$selected_files_markup.'</select>',
    );

    $elements['files']['selected_files_value'] = array(
      '#type' => 'hidden',
      '#value' => $selected_files_string,
    );

    $elements['files']['remove_files'] = array(
      '#type' => 'markup',
      '#value' => '<input type="button" id="edit-remove-files" value="Remove selected files" class="form-submit" />',
    );

    drupal_add_js(drupal_get_path('module', 'import_export') . '/includes/js/file_autocomplete.js');
    drupal_add_js(array('import_export' => array('files' => $files)), 'setting');

    return $elements;
  }

  public function process_profile_basic_settings_form(&$form_state, $form, $old_profile = NULL) {
    // should return a data structure to add to the import_export_profile object
    $files = array();
    $fids = explode(',', $form_state['clicked_button']['#post']['file']['files']['selected_files_value']);

    foreach ((array)$fids as $fid) {
      if (!((int)$fid)) {
        continue;
      }

      $files[$fid] = array(
        $this->id_field() => $fid,
        'update_existing' => first_existing($old_profile->data[$this->my_key()][$fid]['update_existing'], FALSE),
      );
    }
    return $files;
  }

  public function profile_advanced_settings_form(&$form_state, $form, $profile) {
    $elements = array();
    $files = $this->_get_all_valid_files();

    if (count($profile->data[$this->my_key()]) > 0) {
      $elements['files'] = array(
        '#type' => 'fieldset',
        '#title' => 'Files',
      );
    }

    foreach ((array)$profile->data[$this->my_key()] as $fid => $settings) {
      $elements['files'][$fid]['update_existing'] = array(
        '#type' => 'checkbox',
        '#title' => 'Update existing file: "'.$files[$fid].'" (fid: '.$fid.')',
        '#description' => 'When this is checked, Import Export will attempt to locate the file from a previous import/export operation that matches this one.  When unchecked, Import Export will create a new file.',
        '#default_value' => $settings['update_existing'],
      );
    }
    return $elements;
  }

  public function process_profile_advanced_settings_form(&$form_state, $form, $old_profile) {
    // should return a data structure to add to the import_export_profile object
    $files = array();
    foreach ((array)$form_state['values'][$this->my_key()]['files'] as $fid => $settings) {
      $files[$fid] = array(
        $this->id_field() => $fid,
        'update_existing' => $settings['update_existing'],
      );
    }
    return $files;
  }

  public function import_settings_form(&$form_state, $form, $import_export_package) {
    // confirm import behavior for files
    $elements = parent::import_settings_form($form_state, $form, $import_export_package);

    foreach ((array)$import_export_package->get($this->my_key()) as $file) {
      $origin_fid = $file->__import_export_metadata[$this->id_field()];
      $readable_filepath = str_replace('#FILES_DIRECTORY_PATH#', '{files directory}', $file->filepath);
      $elements[$origin_fid]['import']['#title'] = "Import <strong>{$file->filename}</strong>";
      $elements[$origin_fid]['#prefix'] = '<div class="import-item item-' . $this->my_key() . ' clear-block">';

      if ($file->__import_export_metadata['from nid']) {
        $from_nid = $file->__import_export_metadata['from nid'];
        $nodes = $import_export_package->get(import_export_plugin('node')->my_key());
        $from_node_title = $nodes[(int)$from_nid]->title;
      }

      if ($file->__import_export_metadata['from imagebrowser token']) {
        $from = "Automatically added from an ImageBrowser token in body field of node $from_nid ('$from_node_title')<br>";
      }
      else if ($file->__import_export_metadata['from image module']) {
        $from = "Automatically added as a related file of another exported file managed by the Image module (from node $from_nid: '$from_node_title')<br>";
      }

      $elements[$origin_fid]['info'] = array(
        '#prefix' => '<div class="object-info">',
        '#value' =>  "Origin fid: {$origin_fid}<br>
                      $from
                      Filepath: {$readable_filepath}",
        '#suffix' => '</div>',
      );

      if ($file->__import_export_metadata['export mode'] == 'remote') {
        $elements[$origin_fid]['url'] = array(
          '#value' => 'Importing from remote URL: ' . $file->contents,
        );
      }
      else if ($file->__import_export_metadata['export mode'] == 'local') {
        $elements[$origin_fid]['path'] = array(
          '#value' => 'Importing from local path: ' . $file->contents,
        );
      }
    }
    return $elements;
  }

  /* Protected helper functions */

  protected function _get_file_by_fid($fid) {
    $res = db_query("SELECT * FROM {files} WHERE fid = %d", $fid);
    if ($file = db_fetch_object($res)) {
      return $file;
    }
    return FALSE;
  }

  protected function _get_filepath_by_fid($fid) {
    if ($file = $this->_get_file_by_fid($fid)) {
      return $file->filepath;
    }
    return FALSE;
  }

  protected function _get_all_valid_files() {
    $files = array();
    $res = db_query("SELECT fid, filepath FROM {files} ORDER BY filepath ASC");
    while ($file = db_fetch_object($res)) {
      if (file_exists($file->filepath)) {
        $files[$file->fid] = $file->filepath;
      }
    }
    return $files;
  }

  protected function _ensure_path(&$path) {
    if (!$path) {
      return FALSE;
    }

    $dir = file_create_path(dirname($path));
    if ($this->_file_check_directory_recursive($dir, TRUE)) { // create dir and make it writeable
      $path = $dir . DIRECTORY_SEPARATOR . basename($path);
      return TRUE;
    }
    return FALSE;
  }

  // this is adapted from node_export_file's _node_export_file_node_alter()
  protected function _ensure_all_file_paths(&$node) {
    module_load_include('inc', 'import_export', 'import_export.messages');
    
    if (!module_exists('node_export_file')) {
      return FALSE;
    }
    $success = TRUE;

    foreach ($node as $attr => $value) {
      $field = &$node->$attr;

      // Filter this attribute by callback
      //if (!call_user_func_array($attribute_filter_callback, array('attribute' => $attr, 'field' => $field))) {
      //  continue;
      //}

      // Strip off anything that isn't a file, if no files are found skip to
      // the next attribute
      if (is_array($field) || is_object($field)) {
        if (($files = _node_export_file_check_files($field)) == FALSE) {
          continue;
        }
      }
      else {
        continue;
      }
      

      foreach ((array)$files as $i => $file) {
        _node_export_file_unclean_local_file_path($file->filepath);

        if (!$this->_ensure_path($file->filepath)) {
          import_export_add_message('Could not create full path for file @filepath', array('@filepath' => $file->filepath), 'error');
          $success = FALSE;
          continue;
        }

        // filepath may have changed
        if (is_object($field[$i])) {
          $field[$i]->filepath = $file->filepath;
        }
        else if (is_array($field[$i])) {
          $field[$i]['filepath'] = $file->filepath;
        }
        else { // @@TODO: significantly improve error reporting here (if this is even an error... read over code and determine)
          /*print "<pre>Error.\n";
          var_dump($field);
          var_dump($node);
          exit;*/
        }
      }
    }
    return $success;
  }

  // this is taken from file_check_directory() in file.inc -- the only change is that mkdir() is permitted to recurse
  protected function _file_check_directory_recursive(&$directory, $mode = 0, $form_item = NULL) {
    module_load_include('inc', 'import_export', 'import_export.messages');
    
    $directory = rtrim($directory, '/\\');

    // Check if directory exists.
    if (!is_dir($directory)) {
      if (($mode & FILE_CREATE_DIRECTORY) && @mkdir($directory, 0777, TRUE)) {
        import_export_add_message('The directory %directory has been created.', array('%directory' => $directory), 'error');
        @chmod($directory, 0775); // Necessary for non-webserver users.
      }
      else {
        if ($form_item) {
          form_set_error($form_item, t('The directory %directory does not exist.', array('%directory' => $directory)));
        }
        return FALSE;
      }
    }

    // Check to see if the directory is writable.
    if (!is_writable($directory)) {
      if (($mode & FILE_MODIFY_PERMISSIONS) && @chmod($directory, 0775)) {
        import_export_add_message('The permissions of directory %directory have been changed to make it writable.', array('%directory' => $directory), 'error');
      }
      else {
        form_set_error($form_item, t('The directory %directory is not writable', array('%directory' => $directory)));
        watchdog('file system', 'The directory %directory is not writable, because it does not have the correct permissions set.', array('%directory' => $directory), WATCHDOG_ERROR);
        return FALSE;
      }
    }

    if ((file_directory_path() == $directory || file_directory_temp() == $directory) && !is_file("$directory/.htaccess")) {
      $htaccess_lines = "SetHandler Drupal_Security_Do_Not_Remove_See_SA_2006_006\nOptions None\nOptions +FollowSymLinks";
      if (($fp = fopen("$directory/.htaccess", 'w')) && fputs($fp, $htaccess_lines)) {
        fclose($fp);
        chmod($directory .'/.htaccess', 0664);
      }
      else {
        $variables = array('%directory' => $directory, '!htaccess' => '<br />'. nl2br(check_plain($htaccess_lines)));
        form_set_error($form_item, t("Security warning: Couldn't write .htaccess file. Please create a .htaccess file in your %directory directory which contains the following lines: <code>!htaccess</code>", $variables));
        watchdog('security', "Security warning: Couldn't write .htaccess file. Please create a .htaccess file in your %directory directory which contains the following lines: <code>!htaccess</code>", $variables, WATCHDOG_ERROR);
      }
    }

    return TRUE;
  }
}




/*public function export_objects($import_export_profile, $import_export_package, $sparse = FALSE) {
  // this code is adapted from node_export's file export submodule
  $files = $import_export_profile->data[$my_key];
  // export files embedded as imagebrowser tokens in node->body fields
  if (variable_get('import_export_get_imagebrowser_files_from_nodes', TRUE)) {
    $node_key = import_export_get_key_for_data_plugin('import_export_node_data');
    $nids = array_keys($import_export_package->data[$node_key]['nodes']);
    foreach ($nids as $nid) {
      $node = node_load((int)$nid);
      if ($node) {
        $matches = array();
        if ($count = preg_match_all('/\[ibimage==(?P<fid>\d+)/', $node->body, $matches)) {
          foreach ($matches['fid'] as $fid) {
            if ($this->_get_filepath_by_fid($fid)) {
              $files[$fid] = array('fid' => $fid, 'update_existing' => FALSE, 'from nid' => $nid);
            }
            else {
              drupal_set_message("Node $nid contains an imagebrowser token referring to a nonexistent file (fid: $fid)", 'error');
            }
          }
        }
      }
    }
  }



  $assets_path = variable_get('node_export_file_assets_path', '');
  $export_mode = variable_get('node_export_file_mode', 'inline');

  switch ($export_mode) {
    case 'local':
      $export_var = 'node_export_file_path'; break;
    case 'remote':
      $export_var = 'node_export_file_url';  break;
    default:
    case 'inline':
      $export_var = 'node_export_file_data'; break;
  }

  if (_node_export_file_check_assets_path($export_mode, $assets_path) === FALSE) {
    // Don't continue if the assets path is not ready
    return;
  }

  // gather all files to export
  $export = array();
  foreach ($files as $fid => $settings) {
    $res = db_query("SELECT * FROM {files} WHERE fid = %d", $fid);
    if (!($file = db_fetch_object($res))) {
      drupal_set_message('Invalid file ID specified: ' . $fid, 'error');
      continue;
    }

    // When exporting a node ...
    if (!isset($file->filepath) || !is_file($file->filepath)) {
      drupal_set_message(t("File not found: '!path'", array('!path' => $file->filepath)), 'error');
      continue;
    }

    // add node information if this file came from a node->body
    if ($settings['from nid']) {
      $file->from_nid = $settings['from nid'];
    }

    // add file to the export
    $export[$file->fid] = $file;

    // search for original/thumbnail/preview versions of this file and add them too (these are created by image.module)
    if (in_array($file->filename, array('thumbnail', 'preview', '_original'))) {
      $res = db_query("SELECT i2.*, f2.* FROM {files} f
                        JOIN {image} i ON f.fid = i.fid
                        JOIN {image} i2 ON i.nid = i2.nid
                        JOIN {files} f2 ON f2.fid = i2.fid
                        WHERE f.fid = %d AND i2.fid != f.fid", $file->fid);

      while ($row = db_fetch_object($res)) {
        if (!in_array($row->fid, array_keys($files)) && is_file($row->filepath)) {
          unset($row->nid);
          $export[$row->fid] = $row;
        }
        else if (!is_file($row->filepath)) {
          drupal_set_message('Problem detected in either the image table or the files table -- one of the files being exported should have an auxiliary file but it does not exist on disk (fid: '.$row->fid.', filepath: '.$row->filepath.')', 'warning');
        }
      }
    }
  }

  // add meta information and export data to each file in the completed list of files
  foreach ($export as &$file) {
    if (!$sparse) {
      $export_data = _node_export_file_get_export_data($file, $export_mode, $assets_path);
      $file->{$export_var} = $export_data;
    }
    $file->origin_fid = $file->fid;
    $file->fid = NULL;
    _node_export_file_clean_local_file_path($file->filepath);
    $file->update_existing = FALSE;
    if ($settings['update_existing']) {
      $file->update_existing = TRUE;
    }
  }
  return $export;
}*/