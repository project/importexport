<?php

class import_export_node_data extends import_export_data_type {

  public function my_name() {
    return 'node';
  }

  // @@TODO: maybe let other plugins alter this?
  public function get_exportable_object($id, $sparse = FALSE) {
    $nid = (int)$id;
    if ($nid > 0) {
      if ($node = db_fetch_object(db_query("SELECT n.nid, n.title, n.type FROM {node} n WHERE n.nid = %d", $nid))) {
        if (import_export_plugins_are_enabled('site')) {
          $node->sites = sites_get_node_sites($node->nid);
          foreach($node->sites as $sid) {
            $site = _sites_get_site((int)$sid);
            $node->sites[$sid] = new stdClass;
            $node->sites[$sid]->sid = $sid;
            $node->sites[$sid]->title = $site->title;
            $node->sites[$sid]->purl_prefix = $site->purl_prefix;
          }
        }
        return $node;
      }
      else {
        import_export_add_message('Invalid node specified in export profile (nid: @nid)', array('@nid' => $nid), 'error');
        $success = FALSE;
      }
    }
  }
  
  public function prepare_export_ids(&$ids, $export_base) {
    if ($export_base instanceof import_export_profile) { // import_export_profile
      $data = $export_base->data;
    }
    else if (is_array($export_base)) { // settings array
      $data = $export_base;
    }
    else {
      return array();
    }
    
    // nodes from sites
    if (import_export_plugins_are_enabled('site')) {
      foreach ((array)$ids[import_export_plugin('site')->my_key()] as $sid => $settings) {
        // get all nids associated with this site, if this profile specifies to do so
        if ($settings['export_all_nodes'] == TRUE) { // AND all of its nodes should be exported
          $nids = sites_get_nodes_for_site($sid);
          foreach ((array)$nids as $nid) {
            $ids[$this->my_key()][$nid] = array(
              $this->id_field() => $nid,
              'from site' => $sid,
            );
          }
        
          // get all extra fields nodes
          $site = _sites_get_site($sid);
          if (is_object($site->extra_fields) && isset($site->extra_fields->nid)) {
            $ids[$this->my_key()][$site->extra_fields->nid] = array(
              $this->id_field() => $site->extra_fields->nid,
              'extra fields node for site' => $sid,
            );
          }
        }
      }
    }

    // nodes from menus
    if (import_export_plugins_are_enabled('menu')) {
      foreach ((array)$ids[import_export_plugin('menu')->my_key()] as $menu_name => $settings) {
        if ($settings['export_all_nodes'] == TRUE) {
          if ($nids = $this->_import_export_get_nids_in_menu($menu_name)) {
            foreach ((array)$nids as $nid) {
              $ids[$this->my_key()][$nid] = array(
                $this->id_field() => $nid,
                'from menu' => $menu_name,
              );
            }
          }
        }
      }
    }
    parent::prepare_export_ids($ids, $export_base); // this will check all of the objects for validity
    //return $ids;
  }
  
  public function export_package_alter(&$import_export_package, $export_settings, $sparse = FALSE) {
    $exporting_nodes = (bool)(isset($export_settings[$this->my_key()]) && is_array($export_settings[$this->my_key()]) && (count($export_settings[$this->my_key()]) > 0));
    if (!$sparse && $exporting_nodes) {
      // activate node_export and store the code in the package
      $node_code = node_export_node_bulk(array_keys($import_export_package->get($this->my_key())), TRUE);
      $import_export_package->other_luggage[$this->my_key()]['node_code'] = $node_code;
    }
  }

  public function import($import_export_package, $import_settings) {
    ctools_static('import_export : current import : origin namespace', $import_export_package->origin_namespace);
    $nodes_to_create = &ctools_static('import_export : current import : nodes to create', array());
    $nodes_to_update = &ctools_static('import_export : current import : nodes to update', array());
    foreach ((array)$import_settings[$this->my_key()] as $nid => $settings) {
      $new_nid = import_export_translate_id($import_export_package->origin_namespace, $this->id_field(), $nid);
      if ($new_nid && $settings['import_behavior'] == IMPORT_UPDATE_EXISTING) {
        $nodes_to_update[$nid] = $nid;
      }
      else {
        $nodes_to_create[$nid] = $nid;
      }
    }
    $success = TRUE;
    
    // @@TODO: maybe unserialize nodecode, remove nodes that aren't set to import at all, and re-serialize before passing to node_export_import()
    if (isset($import_export_package->other_luggage[$this->my_key()]['node_code']) && is_string($import_export_package->other_luggage[$this->my_key()]['node_code'])) {
      $node_code = $import_export_package->other_luggage[$this->my_key()]['node_code'];
      $success = node_export_import($node_code, 'save-edit', FALSE);
    }
    return $success;
  }

  public function profile_basic_settings_form(&$form_state, $form, $profile = NULL) {
    // should return form elements to be added
    $nids = array();
    $data = $profile->data[$this->my_key()];
    foreach ((array)$data as $nid => $node) {
      $nids[$nid] = $nid; // key by nid to avoid duplication
    }
    
    $elements['extra_nids'] = array(
      '#type' => 'textfield',
      '#title' => 'Extra nids to export',
      '#description' => 'Comma-separated list of nids to export in addition to whatever is exported with the selected sites and menus.',
      '#default_value' => implode(',', $nids),
    );
    return $elements;
  }

  public function process_profile_basic_settings_form(&$form_state, $form, $old_profile = NULL) {
    // should return a data structure to add to the import_export_profile object
    $nodes = array();
    foreach ((array)explode(',', $form_state['values'][$this->my_key()]['extra_nids']) as $nid) {
      $nid = (int)trim($nid);
      if ($nid > 0) {
        $nodes[$nid] = array(
          $this->id_field() => $nid,
          'update_existing' => first_existing($old_profile->data[$this->my_key()][$nid]['update_existing'], FALSE),
        );
      }
    }
    return $nodes;
  }

  public function profile_advanced_settings_form(&$form_state, $form, $profile) {
    // should return form elements to be added
    $elements = array();
    $data = $profile->data[$this->my_key()];
    if (count($data) > 0) {
      $elements['extra_nids'] = array(
        '#type' => 'fieldset',
        '#title' => 'Update existing nodes',
        '#description' => 'If you check the box next to a node, Import Export will attempt to locate the node from a previous import/export operation that matches this one.  When unchecked, Import Export will create a new node.',
      );
    }
    foreach ((array)$data as $nid => $settings) {
      $node = node_load($nid);
      $elements['extra_nids'][$nid]['update_existing'] = array(
        '#type' => 'checkbox',
        '#title' => t('Node "@title" (nid: @nid, type: @type)', array('@title' => $node->title, '@nid' => $node->nid, '@type' => $node->type)),
        '#default_value' => $settings['update_existing'],
      );
    }
    return $elements;
  }

  public function process_profile_advanced_settings_form(&$form_state, $form, $old_profile) {
    // should return a data structure to add to the import_export_profile object
    $nodes = array();
    foreach ((array)$form_state['values'][$this->my_key()]['extra_nids'] as $nid => $settings) {
      $nodes[$nid] = array($this->id_field() => $nid, 'update_existing' => $settings['update_existing']);
    }
    return $nodes;
  }
  
  public function import_settings_form(&$form_state, $form, $import_export_package) {
    $elements = array();
    
    // warn the user if node_export is going to reset anything on the imported nodes
    import_export_check_node_export_reset_settings();
    
    // confirm import behavior for nodes
    if ($import_export_package->has($this->my_key())) {

      $elements['table'] = parent::import_settings_form($form_state, $form, $import_export_package);
      $elements['table']['#theme'] = 'import_export_import_settings_form_node_table';
      
      $node_data = $import_export_package->get($this->my_key());
      $rows = array();
      foreach ((array)$node_data as $node) {
        $origin_nid = $node->__import_export_metadata[$this->id_field()];
        
        $extra = array();
        if ($sid = $node->__import_export_metadata['extra fields node for site']) {
          $extra[] = "Sites Extra Fields node for site $sid";
        }
        if ($menu = $node->__import_export_metadata['from menu']) {
          $extra[] = "Automatically exported from menu '$menu'";
        }
        if ($sid = $node->__import_export_metadata['from site']) {
          $extra[] = "Automatically exported from site $sid";
        }
        
        $elements['table'][$origin_nid]['nid'] = array(
          '#value' => $origin_nid,
        );
        $elements['table'][$origin_nid]['title'] = array(
          '#value' => $node->title,
        );
        
        // add sites column if sites data plugin is enabled
        if (import_export_plugins_are_enabled('site')) {
          $sites = '';
          foreach ((array)$node->sites as $site) {
            $sites .= "{$site->purl_prefix}/{$site->sid}<br>";
          }
          $elements['table'][$origin_nid]['sites'] = array(
            '#value' => $sites,
          );
        }
        
        $elements['table'][$origin_nid]['extra'] = array(
          '#value' => '<ul><li>' . implode('</li><li>', $extra) . '</li></ul>',
        );
        
        $elements['table'][$origin_nid]['exists'] = array(
          '#value' => (import_export_translate_id($import_export_package->origin_namespace, $this->id_field(), $origin_nid) ? 'yes' : 'no'),
        );
      }
    }
    
    return $elements;
  }
  
  public function process_import_settings_form(&$form_state, $form) {
    return $form_state['values'][$this->my_key()]['table'];
  }
  
  /* Private helper functions */
  
  private function _import_export_get_nids_in_menu($menu_name) {
    if ($items = menu_tree_all_data($menu_name)) {
      $nids = $this->_import_export_get_nids_in_menu_recurse($items);
      return $nids;
    }
    return array();
  }

  private function _import_export_get_nids_in_menu_recurse($items) {
    $nids = array();
    foreach ((array)$items as $item) {
      $link = $item['link'];
      if ($link['router_path'] == 'node/%') {
        $nid = (int)substr($link['link_path'], 5);
        if ($nid > 0) {
          $nids[$nid] = $nid;
        }
      }

      if ($item['below']) {
        $nids = array_merge($nids, $this->_import_export_get_nids_in_menu_recurse($item['below']));
      }
    }
    return $nids;
  }
}



