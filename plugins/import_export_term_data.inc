<?php

class import_export_term_data extends import_export_data_type {

  public function my_name() {
    return 'term';
  }

  public function get_exportable_object($id, $sparse = FALSE) {
    $term = taxonomy_get_term((int)$id);
    $term->__import_export_metadata = array('vid' => $term->vid);
    if (!$sparse) {
      $term->__import_export_metadata['parent'] = array_keys(taxonomy_get_parents($term->tid));
      $term->__import_export_metadata['synonyms'] = array_keys(taxonomy_get_related($term->tid));
      $term->__import_export_metadata['relations'] = array_keys(taxonomy_get_synonyms($term->tid));
    }
    return $term;
  }
  
  protected function _sort_terms_by_hierarchy($unordered) {
    $ordered = array();
    $unordered_copy = unserialize(serialize($unordered)); // hackish full clone ensuring no lingering references

    while (count($ordered) < count($unordered)) {
      foreach ($unordered as $term) {
        if (count((array)$term->__import_export_metadata['parent']) == 0 && !in_array($term->__import_export_metadata['tid'], $ordered)) {
          $ordered[] = $term->__import_export_metadata['tid'];
        }
      }
      foreach ($unordered as $i => $term) {
        foreach ((array)$term->__import_export_metadata['parent'] as $j => $tid) {
          if (in_array($tid, $ordered)) {
            unset($unordered[$i]->__import_export_metadata['parent'][$j]);
          }
        }
      }
    }

    $to_return = array();
    foreach ($ordered as $tid) {
      $to_return[$tid] = $unordered_copy[$tid];
    }
    return $to_return;
  }
  
  public function import($import_export_package, $import_settings) {
    $all_success = TRUE;
    $data = $import_export_package->get($this->my_key());
    $sorted = $this->_sort_terms_by_hierarchy($data);

    foreach ($sorted as $tid => $term) {
      $origin_id = $term->__import_export_metadata[$this->id_field()];
      $term = $term->__import_export_metadata;
      
      // translate vid
      if ($dest_vid = import_export_translate_id($import_export_package->origin_namespace, 'vid', $term['vid'])) {
        $term['vid'] = $dest_vid;
      }
      
      // translate parent, related, and synonym tids
      foreach (array('parent', 'relations', 'synonyms') as $type) {
        foreach ($term[$type] as $i => $p_tid) {
          if ($dest_p_tid = import_export_translate_id($import_export_package->origin_namespace, 'tid', $p_tid)) {
            $term[$type][$i] = $dest_p_tid;
          }
        }
      }
      
      // UPDATE EXISTING
      $dest_id = import_export_translate_id($import_export_package->origin_namespace, $this->id_field(), $origin_id);
      $updated = FALSE;
      if ($dest_id && $import_settings[$this->my_key()][$origin_id]['import_behavior'] == IMPORT_UPDATE_EXISTING) {
        $updated = TRUE;
        $term['tid'] = $dest_id;
      }
      // CREATE NEW
      else {
        unset($term['tid']);
      }

      taxonomy_save_term($term);
      
      if (!$term['tid']) {
        $all_success = FALSE;
        import_export_add_message('Error saving term record to the database (origin tid: @origin_id, term: @term)', array('@origin_id' => $origin_id, '@term' => $term['name']), 'error');
      }
      else {
        if ($updated) {
          // if a term was created successfully, add the id translation
          import_export_add_id_translation($import_export_package->origin_namespace, $this->id_field(), $origin_id, $term['tid']);
        }
      }
    }
    return $all_success;
  }

  public function profile_basic_settings_form(&$form_state, $form, $profile = NULL) {
    // should return form elements to be added
    $elements = array();
    $vocabularies = taxonomy_get_vocabularies();
    
    foreach ($vocabularies as $voc) {
      $options = array();
      $default_value = array();
      
      $terms = taxonomy_get_tree($voc->vid);
      foreach ($terms as $term) {
        $options[$term->tid] = $term->name;
      }
      foreach ((array)$profile->data[$this->my_key()] as $tid => $term) {
        if (in_array($tid, array_keys($options))) { // i.e., if this term is in the current vocab
          $default_value[$tid] = $tid;
        }
      }
      
      $elements["terms_voc_{$voc->vid}"] = array(
        '#type' => 'select',
        '#title' => 'Individual terms from the "' . $voc->name . '" vocabulary',
        '#multiple' => TRUE,
        '#size' => 10,
        '#options' => $options,
        '#default_value' => $default_value,
      );
    }

    return $elements;
  }

  public function process_profile_basic_settings_form(&$form_state, $form, $old_profile = NULL) {
    // should return a data structure to add to the import_export_profile object
    $terms = array();
    foreach ($form_state['values'][$this->my_key()] as $id => $my_values) {
      $vid = (int)substr($id, 10);
      foreach ($my_values as $key => $tid) {
        $terms[$tid] = array(
          $this->id_field() => $tid,
          'update_existing' => first_existing($old_profile->data[$this->my_key()][$sid]['update_existing'], FALSE),
        );
      }
    }
    return $terms;
  }

  public function profile_advanced_settings_form(&$form_state, $form, $profile) {
    foreach ($profile->data[$this->my_key()] as $tid => $settings) {
      $term = taxonomy_get_term($tid);
      $elements[$sid]['update_existing'] = array(
        '#type' => 'checkbox',
        '#title' => 'Update existing term: "' . $term->title . '" (tid: ' . $tid . ', vid: ' . $term->vid . ')',
        '#description' => 'When this is checked, Import Export will attempt to locate the term from a previous import/export operation that matches this one.  When unchecked, Import Export will create a new term.',
        '#default_value' => $settings['update_existing'],
      );
    }

    return $elements;
  }

  public function process_profile_advanced_settings_form(&$form_state, $form, $old_profile) {
    // should return a data structure to add to the import_export_profile object
    $terms = array();
    $my_values = $form_state['values'][$this->my_key()];
    foreach ($my_values as $tid => $settings) {
      $terms[$tid] = array(
        'update_existing' => $settings['update_existing'],
      );
    }
    return $terms;
  }

  public function import_settings_form(&$form_state, $form, $import_export_package) {
    $elements = parent::import_settings_form($form_state, $form, $import_export_package);

    // confirm import behavior for terms
    if ($import_export_package->has($this->my_key())) {      
      foreach ((array)$import_export_package->get($this->my_key()) as $term) {
        $id = $term->__import_export_metadata[$this->id_field()];
        $elements[$id]['import']['#title'] = "Import <strong>{$term->name}</strong> (origin tid: {$id}, origin vid: {$term->__import_export_metadata['vid']})";
      }
    }
    return $elements;
  }
}

