<?php
// @@RECENT-CHANGE

class import_export_variable_data extends import_export_data_type {

  public function my_name() {
    return 'variable';
  }

  public function get_exportable_object($id, $sparse = FALSE) {
    $return = new stdClass;
    $return->name = $id;
    $return->value = variable_get($id, NULL);
    return $return;
  }

  public function import($import_export_package, $import_settings) {
    foreach ((array)$import_export_package->get($this->my_key()) as $id => $variable) {
      $origin_id = $variable->__import_export_metadata[$this->id_field()];
      variable_set($origin_id, $variable->value);
    }
    return TRUE;
  }

  public function profile_basic_settings_form(&$form_state, $form, $profile = NULL) {
    // should return form elements to be added
    $elements = array();

    $variables = array();
    $q = "SELECT name FROM {variables}";
    $res = db_query($q);
    while ($name = db_result($res)) {
      $variables[$name] = $name;
    }

    $default = array();
    foreach ((array)$profile->data[$this->my_key()] as $id => $variable) {
      $default[$id] = $id;
    }

    $elements['variables'] = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => 'Variables to export',
      '#description' => 'Select the variables to export.',
      '#options' => $variables,
      '#default_value' => $default,
    );

    return $elements;
  }

  public function process_profile_basic_settings_form(&$form_state, $form, $old_profile = NULL) {
    // should return a data structure to add to the import_export_profile object
    $variables = array();
    $my_values = $form_state['values'][$this->my_key()]['variables'];
    foreach ((array)$my_values as $key => $id) {
      $variables[$id] = array(
        $this->id_field() => $id,
      );
    }
    return $variables;
  }

  public function profile_advanced_settings_form(&$form_state, $form, $profile) {
    return array();
  }

  public function process_profile_advanced_settings_form(&$form_state, $form, $old_profile) {
    // should return a data structure to add to the import_export_profile object
    return $old_profile->get($this->my_key());
  }

  public function import_settings_form(&$form_state, $form, $import_export_package) {
    $elements = parent::import_settings_form($form_state, $form, $import_export_package);

    // confirm import behavior for variables
    if ($import_export_package->has($this->my_key())) {
      foreach ((array)$import_export_package->get($this->my_key()) as $variable) {
        $id = $variable->__import_export_metadata[$this->id_field()];
        $elements[$id]['import']['#title'] = "Import <strong>{$variable->name}</strong>";
      }
    }
    return $elements;
  }
}

