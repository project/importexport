<?php

class import_export_menu_data extends import_export_data_type {
  // @@TODO: use prepare_export_ids to add menus that are inscribed into sites records

  public function my_name() {
    return 'menu';
  }

  /**
   * get_exportable_object()
   *
   * @param $id
   * @param $sparse
   *
   * @return stdClass object or FALSE
   * @author Bryn Bellomy
   **/
  public function get_exportable_object($id, $sparse = FALSE) {
    $menu_name = $id;
    if ($m = menu_load($menu_name)) {
      // add any extra fields that may be useful when importing the object
      $obj->__import_export_metadata['title'] = $m['title'];
      $obj->__import_export_metadata['description'] = $m['description'];
      if (!$sparse) {
        $obj->__import_export_metadata['tree'] = menu_tree_all_data($menu_name);
      }
      return $obj;
    }
    else {
      return FALSE;
    }
  }

  public function import($import_export_package, $import_settings) {
    // @@TODO: implement different menu name in src and dest via origin_namespace
    foreach ((array)$import_export_package->get($this->my_key()) as $menu_name => $menu) {
      $this->_delete_menu($menu->__import_export_metadata[$this->id_field()]);
      $this->_create_menu($menu->__import_export_metadata[$this->id_field()], $menu->__import_export_metadata['title'], $menu->__import_export_metadata['description']);
      $this->_import_recurse($import_export_package->origin_namespace, $menu->__import_export_metadata['tree']);
    }
    return TRUE; // @@TODO: remove and do real error checking
  }

  private function _import_recurse($origin_namespace, $items, $parent = NULL) {
    foreach ((array)$items as $item) {
      $link = $item['link'];
      $children = $item['below'];

      if ($parent) {
        $link['plid'] = $parent['mlid'];
      }
      $link['mlid'] = 0; // reset the mlid
      //@@TODO: for updating individual menu items, translate mlids and parent mlids, etc.

      // get rid of the parent hierarchy -- menu_link_save should build this for us
      for ($i = 1; $i < 10; $i++) {
        unset($link["p$i"]);
      }
      unset($link['in_active_trail']);
      unset($link['depth']);

      // if this item points to a node, replace the old nid with the equivalent new nid.
      // if the node wasn't part of the node import, keep the existing nid, as it probably
      // refers to a node that has already been imported.
      if ($link['router_path'] == 'node/%' && import_export_plugins_are_enabled('node')) {
        $origin_nid = substr($link['link_path'], 5);
        $node_id_field = import_export_plugin('node')->id_field();
        $nid = (import_export_translate_id($origin_namespace, $node_id_field, $origin_nid) ? import_export_translate_id($origin_namespace, $node_id_field, $origin_nid) : $origin_nid);
        $link['link_path'] = "node/$nid";
        $link['href'] = "node/$nid";
      }
      if ($mlid = menu_link_save($link)) {
        $link['mlid'] = $mlid; // @@TODO if menu link is new, store translation
        if ($children) {
          $this->_import_recurse($origin_namespace, $children, $link);
        }
      }
      else {
        import_export_add_message('Error importing menu item "!link_path"', array('!link_path' => $link['link_path']), 'error');
      }
    }
  }

  public function profile_basic_settings_form(&$form_state, $form, $profile = NULL) {
    $menu_defaults = array();
    if ($profile) {
      foreach ((array)$profile->data[$this->my_key()] as $menu_name => $menu) {
        $menu_defaults[$menu_name] = $menu_name;
      }
    }

    $elements['menus'] = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => 'Menus to export',
      '#description' => 'Select the menus to export.',
      '#options' => menu_get_menus(),
      '#default_value' => $menu_defaults,
    );

    return $elements;
  }

  public function process_profile_basic_settings_form(&$form_state, $form, $old_profile = NULL) {
    // should return a data structure to add to the import_export_profile object
    $menus = array();
    $my_values = $form_state['values'][$this->my_key()]['menus'];

    foreach ((array)$my_values as $menu_name) {
      $menus[$menu_name] = array(
        $this->id_field() => $menu_name,
        'export_all_nodes' => first_existing($old_profile->data[$this->my_key()][$menu_name]['export_all_nodes'], TRUE),
      );
    }
    return $menus;
  }

  public function profile_advanced_settings_form(&$form_state, $form, $profile) {
    $elements['menus'] = array(
      '#type' => 'fieldset',
      '#title' => 'Menus',
    );

    $data = $profile->data[$this->my_key()];
    foreach ((array)$data as $menu_name => $menu) {
      $elements['menus'][$menu_name]['export_all_nodes'] = array(
        '#type' => 'checkbox',
        '#title' => t('Export all nodes in menu "@menu_name"', array('@menu_name' => $menu_name)),
        '#default_value' => $menu['export_all_nodes'],
      );
    }
    return $elements;
  }

  public function process_profile_advanced_settings_form(&$form_state, $form, $old_profile) {
    // should return a data structure to add to the import_export_profile object
    $menus = array();
    $my_values = $form_state['values'][$this->my_key()];
    foreach ((array)$my_values['menus'] as $menu_name => $settings) {
      $menus[$menu_name] = array(
        $this->id_field() => $menu_name,
        'export_all_nodes' => $settings['export_all_nodes'],
      );
    }
    return $menus;
  }

  public function import_settings_form(&$form_state, $form, $import_export_package) {
    $elements = parent::import_settings_form($form_state, $form, $import_export_package);

    // confirm import behavior for menus
    if ($import_export_package->has($this->my_key())) {
      foreach ((array)$import_export_package->get($this->my_key()) as $menu) {
        $menu_name = $menu->__import_export_metadata[$this->id_field()];
        $elements[$menu_name]['import']['#title'] = "Import <strong>{$menu->__import_export_metadata['title']}</strong> (menu_name: {$menu_name})";

        // check for existing menu with this name
        if (menu_load($menu_name)) { // @@TODO: make it possible to create a new menu
          $elements[$menu_name]['warning_' . $menu_name] = array(
            '#value' => t('WARNING!  This menu already exists in the database and will be overwritten.'),
          );
        }
      }
    }
    return $elements;
  }

  private function _create_menu($menu_name, $human_name, $description) {
    $path = 'admin/build/menu-customize/';
    if (substr($menu_name, 0, 5) != 'menu-') {
      $menu_name = 'menu-'. $menu_name; // Add 'menu-' to the menu name to help avoid name-space conflicts.
    }

    $link['link_title'] = $human_name;
    $link['link_path'] = $path . $menu_name;
    $link['router_path'] = $path .'%';
    $link['module'] = 'menu';
    $link['plid'] = db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = '%s' AND module = '%s'", 'admin/build/menu', 'system'));
    menu_link_save($link);

    db_query("INSERT INTO {menu_custom} (menu_name, title, description) VALUES ('%s', '%s', '%s')", $menu_name, $human_name, $description);
  }

  private function _delete_menu($menu_name) {
    // System-defined menus may not be deleted - only menus defined by the menu module.
    if (in_array($menu_name, menu_list_system_menus())  || !db_result(db_query("SELECT COUNT(*) FROM {menu_custom} WHERE menu_name = '%s'", $menu_name))) {
      return;
    }
    // Reset all the menu links defined by the system via hook_menu.
    $result = db_query("SELECT * FROM {menu_links} ml INNER JOIN {menu_router} m ON ml.router_path = m.path WHERE ml.menu_name = '%s' AND ml.module = 'system' ORDER BY m.number_parts ASC", $menu_name);
    while ($item = db_fetch_array($result)) {
      menu_reset_item($item);
    }
    // Delete all links to the overview page for this menu.
    $result = db_query("SELECT mlid FROM {menu_links} ml WHERE ml.link_path = '%s'", 'admin/build/menu-customize/'. $menu_name);
    while ($m = db_fetch_array($result)) {
      menu_link_delete($m['mlid']);
    }
    // Delete all the links in the menu and the menu from the list of custom menus.
    db_query("DELETE FROM {menu_links} WHERE menu_name = '%s'", $menu_name);
    db_query("DELETE FROM {menu_custom} WHERE menu_name = '%s'", $menu_name);
    // Delete all the blocks for this menu.
    db_query("DELETE FROM {blocks} WHERE module = 'menu' AND delta = '%s'", $menu_name);
    db_query("DELETE FROM {blocks_roles} WHERE module = 'menu' AND delta = '%s'", $menu_name);
    menu_cache_clear_all();
    cache_clear_all();
  }
}




// should accept the component of the import_export_profile object that it created in 'process_profile_basic_settings_form' and 'process_profile_advanced_settings_form'
// should return a data structure to be added to the import_export_package object, which will need to process in import()
/*public function export_objects($import_export_profile, $import_export_package, $sparse = FALSE) {
  $export = array();
  require_once './' . drupal_get_path('module', 'import_export') . '/includes/menu_wrapper.inc';
  foreach ((array)$import_export_profile->data[$my_key] as $menu_name => $settings) {

  }
  return $export;
}*/