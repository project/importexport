<?php

class import_export_site_data extends import_export_data_type {

  public function my_name() {
    return 'site';
  }

  public function get_exportable_object($id, $sparse = FALSE) {
    $site = _sites_get_site((int)$id);
    if (is_object($site->extra_fields) && isset($site->extra_fields->nid)) {
      // if sparse, remove everything except the nid from the extra_fields node attached to the site
      // (the node will be imported by the node plugin)
      $nid = $site->extra_fields->nid;
      $site->extra_fields = new stdClass;
      $site->extra_fields->nid = $nid;
    }
    return $site;
  }

  public function import($import_export_package, $import_settings) {
    $all_success = TRUE;

    foreach ((array)$import_export_package->get($this->my_key()) as $sid => $site) {
      $origin_sid = $site->__import_export_metadata[$this->id_field()];
      
      // UPDATE EXISTING
      $new_sid = import_export_translate_id($import_export_package->origin_namespace, $this->id_field(), $origin_sid);
      $updated = FALSE;
      if ($new_sid && $import_settings[$this->my_key()][$origin_sid]['import_behavior'] == IMPORT_UPDATE_EXISTING) {
        $updated = TRUE;
        $site->sid = $new_sid;
      }
      // CREATE NEW
      else {
        $site->sid = NULL;
      }

      $success = $site->save();
      if ($success === FALSE || !$site->sid) {
        $all_success = FALSE;
        import_export_add_message('Error saving site record to the database (origin sid: @origin_sid, title: @title)', array('@origin_sid' => $origin_sid, '@title' => $site->title), 'error');
      }
      else if ($success && $site->sid) {
        if ($updated) {
          // if a site was created successfully, add the id translation
          import_export_add_id_translation($import_export_package->origin_namespace, $this->id_field(), $origin_sid, $site->sid);
          
        }
        else {
          
        }
      }
    }
    return $all_success;
  }

  public function profile_basic_settings_form(&$form_state, $form, $profile = NULL) {
    // should return form elements to be added
    $elements = array();

    $sites = _sites_get_sites();
    $site_options = array();
    foreach ((array)$sites as $site) {
      $site_options[$site->sid] = $site->name;
    }

    $sids = array();
    foreach ((array)$profile->data[$this->my_key()] as $sid => $site) {
      $sids[$sid] = $sid;
    }

    $elements['sites'] = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => 'Sites to export',
      '#description' => 'Select the sites to export.',
      '#options' => $site_options,
      '#default_value' => $sids,
    );

    return $elements;
  }

  public function process_profile_basic_settings_form(&$form_state, $form, $old_profile = NULL) {
    // should return a data structure to add to the import_export_profile object
    $sites = array();
    $my_values = $form_state['values'][$this->my_key()]['sites'];
    foreach ((array)$my_values as $key => $sid) {
      $sites[$sid] = array(
        $this->id_field() => $sid,
        'update_existing' => first_existing($old_profile->data[$this->my_key()][$sid]['update_existing'], FALSE),
        'export_all_nodes' => first_existing($old_profile->data[$this->my_key()][$sid]['export_all_nodes'], TRUE),
        'export_all_menus' => first_existing($old_profile->data[$this->my_key()][$sid]['export_all_menus'], TRUE),
      );
    }
    return $sites;
  }

  public function profile_advanced_settings_form(&$form_state, $form, $profile) {
    $sites = _sites_get_sites();

    foreach ((array)$profile->data[$this->my_key()] as $sid => $settings) {
      $site = $sites[$sid];
      $elements[$sid] = array(
        '#type' => 'fieldset',
        '#title' => 'Site: ' . $sites[$sid]->title,
      );
      $elements[$sid]['update_existing'] = array(
        '#type' => 'checkbox',
        '#title' => 'Update existing site: "'.$sites[$sid]->title.'" (sid: '.$sid.')',
        '#description' => 'When this is checked, Import Export will attempt to locate the site from a previous import/export operation that matches this one.  When unchecked, Import Export will create a new site.',
        '#default_value' => $settings['update_existing'],
      );
      if (import_export_plugins_are_enabled('node')) {
        $elements[$sid]['export_all_nodes'] = array(
          '#type' => 'checkbox',
          '#title' => 'Export all nodes tagged with site "' . $sites[$sid]->title . '"',
          '#default_value' => $settings['export_all_nodes'],
        );
      }
      if (import_export_plugins_are_enabled('menu')) {
        $elements[$sid]['export_all_menus'] = array(
          '#type' => 'checkbox',
          '#title' => 'Export all menus tagged with site "' . $sites[$sid]->title . '"',
          '#default_value' => $settings['export_all_menus'],
        );
      }
    }

    return $elements;
  }

  public function process_profile_advanced_settings_form(&$form_state, $form, $old_profile) {
    // should return a data structure to add to the import_export_profile object
    $sites = array();
    $my_values = $form_state['values'][$this->my_key()];
    foreach ((array)$my_values as $sid => $settings) {
      $sites[$sid] = array(
        'update_existing' => $settings['update_existing'],
        'export_all_nodes' => first_existing($settings['export_all_nodes'], $old_profile->data[$this->my_key()][$sid]['export_all_nodes'], TRUE),
        'export_all_menus' => first_existing($settings['export_all_menus'], $old_profile->data[$this->my_key()][$sid]['export_all_menus'], TRUE),
      );
    }
    return $sites;
  }

  public function import_settings_form(&$form_state, $form, $import_export_package) {
    $elements = parent::import_settings_form($form_state, $form, $import_export_package);

    // confirm import behavior for sites
    if ($import_export_package->has($this->my_key())) {
      
      foreach ((array)$import_export_package->get($this->my_key()) as $site) {
        $id = $site->__import_export_metadata[$this->id_field()];
        $elements[$id]['import']['#title'] = "Import <strong>{$site->title}</strong> (origin sid: {$id}, PURL prefix: {$site->purl_prefix})";
      }
    }
    return $elements;
  }
}

