<?php

interface import_export_data_interface {
  public function my_name();
  public function id_field();
  public function my_key();
  public function get_exportable_object($id, $sparse = FALSE);
  public function export_package_alter(&$import_export_package, $export_base, $sparse = FALSE);
  public function import($import_export_package, $import_settings);
  public function profile_basic_settings_form(&$form_state, $form, $profile = NULL); // should return form elements to be added
  public function process_profile_basic_settings_form(&$form_state, $form, $old_profile = NULL); // should return a data structure to add to the import_export_profile object
  public function profile_advanced_settings_form(&$form_state, $form, $profile); // should return form elements to be added
  public function process_profile_advanced_settings_form(&$form_state, $form, $old_profile); // should return a data structure to add to the import_export_profile object
  public function should_export_object($settings);
  public function should_import_object($settings);
  public function valid_id($id);
  public function prepare_export_ids(&$ids, $export_base);
  public function export_objects($export_settings, $sparse = FALSE);
  public function prepare_import_package(&$import_export_package, $method, $import_settings, $import_data);
  public function import_settings_form(&$form_state, $form, $import_export_package);
  public function process_import_settings_form(&$form_state, $form);
}