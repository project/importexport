<?php

// $Id$

/**
 * An export profile is a group of settings from which an export
 * package can be generated.
 * (brynbellomy)
 */

class import_export_profile {
  var $epid; // int
  var $description; // string
  var $data; // // $profile->data['data_type'][$id] = array('id' => $id, 'some setting' => $some_setting);

  function __construct($epid = NULL) {
    $this->data = array();
    if ($epid) {
      $this->load($epid);
    }
  }
  
  function load($epid) {
    $res = db_query("SELECT * FROM {import_export_profiles} WHERE epid = %d", $epid);
    $profile = db_fetch_object($res);
    if ($profile && $profile->epid) {
      $this->epid = $profile->epid;
      $this->description = $profile->description;
      $this->data = unserialize($profile->serialized_data);
      drupal_alter('import_export_profile_load', $this);
      return $profile;
    }
    else {
      return FALSE;
    }
  }

  function save() {
    drupal_alter('import_export_profile_presave', $this);
    $success = NULL;
    $profile = new import_export_profile;
    $profile->serialized_data = serialize($this->data);
    $profile->description = $this->description;
    if ($this->epid) {
      $profile->epid = $this->epid;
      $success = drupal_write_record('import_export_profiles', $profile, array('epid'));
    }
    else {
      $success = drupal_write_record('import_export_profiles', $profile);
      if ($profile->epid && $success) {
        $this->epid = $profile->epid;
      }
    }
    if (!$success) {
      drupal_set_message('Failed to write import export profile to DB.', 'error');
      return FALSE;
    }
    return $success;
  }
  
  public static function delete($epid) {
    db_query("DELETE FROM {import_export_profiles} WHERE epid = %d", $epid);
  }
  
  function has($plugin_key) {
    if (is_array($this->data[$plugin_key]) && count($this->data[$plugin_key]) > 0) {
      return TRUE;
    }
    return FALSE;
  }

  function get($plugin_key, $id = NULL) {
    if ($this->has($plugin_key)) {
      if ($id && isset($this->data[$plugin_key][$id])) {
        return $this->data[$plugin_key][$id];
      }
      return $this->data[$plugin_key];
    }
    return array();
  }
  
  function set($plugin_key, $data) {
    $this->data[$plugin_key] = $data;
  }
}
