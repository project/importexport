<?php

// $Id$

/**
 * A package is a data record of various kinds of user content.  It is used
 * for cloning, importing, and exporting data.
 * (brynbellomy)
 */

class import_export_package {
  var $origin_namespace;
  var $data;
  var $other_luggage;

  function __construct($construct_base = NULL, $sparse = FALSE) {
    module_load_include('inc', 'import_export', 'classes/import_export_profile');
    $this->data = array();

    // create from an array of IDs => settings
    if (is_array($construct_base)) {
      $this->origin_namespace = import_export_get_namespace();
      $this->data = import_export_plugin_invoke_all('export_objects', 'export', $construct_base, $sparse);

      import_export_plugin_invoke_all_by_ref('export_package_alter', 'export', $this, $construct_base, $sparse);
      drupal_alter('import_export_package', $this);
    }
    // create from serialized data
    else if (is_string($construct_base)) {
      $this->load($construct_base);
    }
  }
  
  function __clone() {
    // hackish for the sake of thoroughness and brevity -- this will break all references instantly without any recursion
    foreach ((array)$this->data as $key => $val) {
      if (is_object($val) || (is_array($val))) {
        $this->data[$key] = unserialize(serialize($val));
      }
    }
    // proper OOP approach
    /*foreach ($this->data as $plugin_key => $objects) {
      foreach ($objects as $id => $object_or_settings) {
        if (is_object($object_or_settings)) {
          $this->data[$plugin_key][$id] = clone $this->data[$plugin_key][$id];
        }
      }
    }*/
  }

  public static function decode($data) {
    import_export_load_unserialize_dependencies();    
    return unserialize($data);
  }

  public static function encode($data) {
    return serialize($data);
  }

  function load($encoded_data) {
    $package = self::decode($encoded_data);
    if ($package) {
      $this->data = $package->data;
      $this->origin_namespace = $package->origin_namespace;
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  function has($plugin_key) {
    if (is_array($this->data[$plugin_key]) && count($this->data[$plugin_key]) > 0) {
      return TRUE;
    }
    return FALSE;
  }

  function get($plugin_key, $id = NULL) {
    if ($this->has($plugin_key)) {
      if ($id) {
        if (isset($this->data[$plugin_key][$id])) {
          return $this->data[$plugin_key][$id];
        }
        return NULL;
      }
      return $this->data[$plugin_key];
    }
    return NULL;
  }

  function set($plugin_key, $data) {
    $this->data[$plugin_key] = $data;
  }
  
  function setSingle($plugin_key, $id, $data) {
    $this->data[$plugin_key][$id] = $data;
  }
  
  function remove($plugin_key, $id = NULL) {
    if ($id) {
      unset($this->data[$plugin_key][$id]);
    }
    else {
      unset($this->data[$plugin_key]);
    }
  }

  function get_export_code($encode = TRUE) {
    // ensure namespace is still set
    $this->origin_namespace = import_export_get_namespace();
    if ($encode) {
      return import_export_package::encode($this);
    }
    return $this;
  }
}
