<?php

function import_export_get_data_plugin_name_by_key($key) {
  static $keys_to_names = NULL;
  if (!isset($keys_to_names)) {
    $plugins = import_export_get_data_plugin_info_all();
    foreach ($plugins as $name => $plugin) {
      $keys_to_names[$plugin['key']] = $name;
    }
  }
  return (isset($keys_to_names[$key]) ? $keys_to_names[$key] : FALSE);
}

function import_export_plugin($plugin_name, $type = 'data') {
  static $plugins = array();
  if (!isset($plugins[$plugin_name])) {
    ctools_include('plugins');
    if (import_export_plugins_are_enabled($plugin_name)) {
      $plugin_info = import_export_get_data_plugin_info($plugin_name);
      ctools_plugin_get_class($plugin, 'handler');
      if ($class = ctools_plugin_get_class($plugin_info, 'handler')) {
        $plugins[$plugin_name] = new $class();
      }
    }
    
    if (!isset($plugins[$plugin_name])) {
      module_load_include('inc', 'import_export', 'plugins/import_export_data_type');
      $plugins[$plugin_name] = new import_export_data_type_nothing;
      throw new PluginNotLoadedException("Plugin $plugin_name is not loaded.");
    }
  }
  return $plugins[$plugin_name];
}

// @@TODO
/*function import_export_plugins() {
  if (func_num_args() > 0) {
    $args = func_get_args();
    if (is_array($args[0])) { // to specify the plugin type: import_export_plugins(array(plugin1, plugin2), 'type')
      $type = first_existing($args[1], 'data');
      $plugins = $args[0];
    }
    else { // otherwise, data_types is assumed when you simply call import_export_plugins() or import_export_plugins(plugin1, plugin2)
      $type = 'data_types';
      $plugins = $args;
    }
  }
  else {
    $plugins = array_combine(array_keys(import_export_get_data_plugin_info_all()), array_keys(import_export_get_data_plugin_info_all()));
  }
  
  // load each plugin
  foreach ($plugins as $name => $x) {
    $plugins[$name] = import_export_plugin($name);
  }
  module_load_include('inc', 'import_export', 'classes/import_export_plugin_controller');
  return new import_export_plugin_controller($plugins);
}*/

function import_export_get_data_plugin_info($plugin_name, $reset = FALSE) {
  $plugins = import_export_get_data_plugin_info_all('import', $reset);
  return $plugins[$plugin_name];
}

function import_export_get_data_plugin_info_all($op = 'import', $reset = FALSE) {
  ctools_include('plugins');
  $cache = &ctools_static(__FUNCTION__, NULL);
  ctools_plugin_load_class('import_export', 'data_types', 'import_export_data_type', 'handler', TRUE);
  if ($cache == NULL or $reset === TRUE) {
    ctools_include('plugins');
    $type = 'data_types';
    $plugins = ctools_get_plugins('import_export', $type);
    unset($plugins['import_export_data_interface']); // remove parent class
    unset($plugins['import_export_data_type']); // remove parent class
    unset($plugins['__nothing']); // remove 'nothing' class
    // remove plugins that depend on modules that are not enabled
    $plugins_copy = (array)$plugins;
    foreach ($plugins_copy as $name => $plugin) {
      foreach ($plugin['module dependencies'] as $module) {
        if (!module_exists($module)) {
          unset($plugins[$name]);
          continue 2;
        }
      }
    }
    $cache['import'] = _import_export_sort_plugins_by_dependencies('import', $plugins);
    $cache['export'] = _import_export_sort_plugins_by_dependencies('export', $plugins);
    drupal_alter('import_export_plugin_info_all', $cache, $op, $type);
  }
  return $cache[$op];
}

function _import_export_sort_plugins_by_dependencies($op, $unordered) {
  $ordered = array();
  $unordered_copy = (array)$unordered;

  // sort
  while (count($ordered) < count($unordered)) {
    foreach ($unordered as $name => $plugin) {
      if (count((array)$plugin["$op dependencies"]) == 0 && !in_array($name, $ordered)) {
        $ordered[] = $name;
      }
    }
    foreach ($unordered as $name => $plugin) {
      foreach ((array)$plugin["$op dependencies"] as $i => $dep) {
        if (in_array($dep, $ordered) || !array_key_exists($dep, $unordered_copy)) { // if the dependency has already been added, or it's not enabled
          unset($unordered[$name]["$op dependencies"][$i]);
        }
      }
    }
  }

  // apply sort to actual plugins list
  $to_return = array();
  foreach ($ordered as $name) {
    $to_return[$name] = $unordered_copy[$name];
  }
  return $to_return;
}

function import_export_plugins_are_enabled() {
  $args = func_get_args();
  $plugins = import_export_get_data_plugin_info_all();
  foreach ($args as $plugin_name) {
    if (!isset($plugins[$plugin_name])) {
      return FALSE;
    }
  }
  return TRUE;
}

function import_export_plugin_invoke($plugin_name, $function) {
  $args = func_get_args();
  array_shift($args); // plugin_name
  array_shift($args); // function
  
  // if we don't clone all of the arguments that are objects, they'll end up being passed by reference
  foreach ($args as $i => $arg) {
    if (is_object($arg)) {
      $args[$i] = clone $args[$i];
    }
  }
  
  $plugin = import_export_plugin($plugin_name);
  return call_user_func_array(array($plugin, $function), $args);
}

function import_export_plugin_invoke_by_ref($plugin_name, $function, &$alter) {
  $args = array(&$alter);
  $additional_args = func_get_args();
  array_shift($additional_args); // function
  array_shift($additional_args); // ordering
  array_shift($additional_args); // alter
  
  // if we don't clone all of the arguments that are objects, they'll end up being passed by reference
  foreach ($additional_args as $i => $arg) {
    if (is_object($arg)) {
      $additional_args[$i] = clone $additional_args[$i];
    }
  }
  $args = array_merge($args, $additional_args);
  
  $plugin = import_export_plugin($plugin_name);
  return call_user_func_array(array($plugin, $function), $args);
}

function import_export_plugin_invoke_all($function, $ordering) {
  $args = func_get_args();
  array_shift($args); // function
  array_shift($args); // ordering
  
  // if we don't clone all of the arguments that are objects, they'll end up being passed by reference
  foreach ($args as $i => $arg) {
    if (is_object($arg)) {
      $args[$i] = clone $args[$i];
    }
  }
  
  module_load_include('inc', 'import_export', 'classes/import_export_profile');
  foreach (import_export_get_data_plugin_info_all($ordering) as $name => $plugin) {
    $plugin = import_export_plugin($name);
    $result[$plugin->my_key()] = call_user_func_array(array($plugin, $function), $args);
  }
  return $result;
}


function import_export_plugin_invoke_all_by_ref($function, $ordering, &$alter) {
  $args = array(&$alter);
  $additional_args = func_get_args();
  array_shift($additional_args); // function
  array_shift($additional_args); // ordering
  array_shift($additional_args); // alter
  
  // if we don't clone all of the arguments that are objects, they'll end up being passed by reference
  foreach ($additional_args as $i => $arg) {
    if (is_object($arg)) {
      $additional_args[$i] = clone $additional_args[$i];
    }
  }
  $args = array_merge($args, $additional_args);

  foreach (import_export_get_data_plugin_info_all($ordering) as $name => $plugin) {
    $plugin = import_export_plugin($name);
    $result[$plugin->my_key()] = call_user_func_array(array($plugin, $function), $args);
  }
  return $result;
}


function import_export_get_plugin_info($plugin_name) {
  $info_path = func_get_args();
  $info = NULL;
  $plugin_info = import_export_get_data_plugin_info_all();
  
  while (count($info_path) > 0) {
    $current = array_shift($info_path);
    if (isset($plugin_info[$current])) {
      $plugin_info = $plugin_info[$current];
    }
    else {
      return NULL;
    }
  }
  return $plugin_info;
}

function import_export_load_unserialize_dependencies() {
  $plugin_info = import_export_get_data_plugin_info_all();
  foreach ($plugin_info as $name => $plugin) {
    if (is_array($plugin['unserialize dependencies'])) {
      foreach ($plugin['unserialize dependencies'] as $filepath) {
        require_once './' . $filepath;
      }
    }
  }
  module_load_include('inc', 'import_export', 'classes/import_export_package');
  module_load_include('inc', 'import_export', 'classes/import_export_profile');
}

function import_export_generate_namespace() {
  if (isset($_SERVER['HTTP_HOST'])) {
    return $_SERVER['HTTP_HOST'];
  }
  return variable_get('site_name', 'site-'.time());
}

function import_export_get_namespace() {
  $namespace = variable_get('import_export_namespace', NULL);
  if (!$namespace) {
    $namespace = import_export_generate_namespace();
    variable_set('import_export_namespace', $namespace);
  }
  return $namespace;
}