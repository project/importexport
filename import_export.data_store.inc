<?php

function import_export__data_store__ensure_ready() {
  import_export_load_unserialize_dependencies();
  $cache = cache_get('import_export : current import : data store');
  if ($cache && is_array($cache->data)) {
    $store = $cache->data;
  }
  else {
    $store = array();
  }
  
  // default values
  if (!isset($store['package']) || !($store['package'] instanceof import_export_package)) {
    $store['package'] = new import_export_package;
  }
  
  if (!isset($store['metadata']) || !is_array($store['metadata'])) {
    $store['metadata'] = array('sparse package?' => TRUE);
  }
  else if (!isset($store['metadata']['sparse package?'])) {
    $store['metadata']['sparse package?'] = TRUE;
  }
  
  _import_export__data_store__write($store);
  return $store;
}


function import_export__data_store__clear() {
  cache_clear_all('import_export : current import : data store', 'cache');
}


function import_export__data_store__acquire($method, $import_metadata = NULL, $import_settings = NULL, $plugin_name = NULL) {
  // if the package is no longer sparse, acquisition is finished
  if (import_export__data_store__package_is_sparse() == FALSE) {
    return TRUE;
  }
  
  if ($plugin_name) {
    $plugin = import_export_plugin($plugin_name);
  }
  
  $existing_metadata = import_export__data_store__retrieve('metadata');
  if (!$import_metadata && !$existing_metadata) {
    // no metadata specified, abort
    return FALSE;
  }
  else if ($existing_metadata) {
    if ($import_metadata) {
      $import_metadata += $existing_metadata;
    }
    else {
      $import_metadata = $existing_metadata;
    }
  }
  
  // remote server acquisition
  if ($method == 'server') {
    if ($plugin) {
      $import_settings = array($plugin->my_key() => $import_settings[$plugin->my_key()]);
    }
    
    //$package = import_export_use_service($import_metadata['base url'], 'import_export_package/download', 'php', 'POST', NULL, array('settings' => $import_settings));
    // @@RECENT-CHANGE
    $package = import_export_use_service($import_metadata['base url'], 'import_export/download', 'php', 'POST', NULL, array('settings' => $import_settings));
    
    module_load_include('inc', 'import_export', 'import_export.messages');
    import_export_show_any_messages_in_data($package);
    
    if ($package instanceof import_export_package) {
      if ($plugin) {
        import_export__data_store__store('package', $package, $plugin->my_name());
      }
      else {
        import_export__data_store__store('package', $package);
      }
    }
    else {
      drupal_set_message('import_export__data_store__acquire(): Received package is not of type import_export_package', 'error');
      return FALSE;
    }
  }
  // local file acquisition
  else if ($method == 'file') {
    if ($data = file_get_contents($import_metadata['uploaded file object']->filepath)) {
      $package = new import_export_package;
      if ($package->load($data)) {
        import_export__data_store__store('package', $package);
        import_export__data_store__set_package_not_sparse();
      }
    }
  }
  import_export__data_store__store('metadata', $import_metadata);
  return TRUE; // @@TODO: real success return value
}




function import_export__data_store__retrieve($key = NULL, $plugin_name = NULL) {
  $store = import_export__data_store__ensure_ready();
  
  if ($key == 'package' && isset($store['package']) && $plugin_name && $store['package'] instanceof import_export_package) {
    if (import_export_plugins_are_enabled($plugin_name) == FALSE) {
      return NULL;
    }
    return $store['package']->get(import_export_plugin($plugin_name)->my_key());
  }
  else if (isset($store[$key])) {
    return $store[$key];
  }
  else if ($key === NULL) {
    return $store;
  }
    
  return NULL;
}



function import_export__data_store__retrieve_sparse_package($method) {
  if ($method == 'server') {
    return import_export__data_store__retrieve('package');
  }
  else if ($method == 'file') {
    $success = import_export__data_store__acquire('file');
    if ($success) {
      return import_export__data_store__retrieve('package');
    }
  }
  return NULL;
}



function import_export__data_store__set_package_not_sparse() {
  if (!$metadata = import_export__data_store__retrieve('metadata')) {
    $metadata = array();
  }
  
  $metadata['sparse package?'] = FALSE;
  import_export__data_store__store('metadata', $metadata);
}


function import_export__data_store__package_is_sparse() {
  $metadata = import_export__data_store__retrieve('metadata');
  if (!$metadata || !is_array($metadata) || !isset($metadata['sparse package?'])) {
    return TRUE; // @@TODO: ??  // who knows
  }
  return $metadata['sparse package?'];
}



function import_export__data_store__store($key, $data_to_store, $plugin_name = NULL) {
  $store = import_export__data_store__ensure_ready();
  
  if ($key == 'package' && $plugin_name && isset($store['package']) && $store['package'] instanceof import_export_package && $data_to_store instanceof import_export_package) {
    if (import_export_plugins_are_enabled($plugin_name) == FALSE) {
      return;
    }
    $plugin_key = import_export_plugin($plugin_name)->my_key();
    $data = $data_to_store->get($plugin_key);
    $other_luggage = $data_to_store->other_luggage[$plugin_key];
    $store['package']->set($plugin_key, $data);
    $store['package']->other_luggage[$plugin_key] = $other_luggage;
    _import_export__data_store__write($store);
  }
  else if ($key !== NULL) {
    $store[$key] = $data_to_store;
    _import_export__data_store__write($store);
  }
  else if ($key === NULL) {
    $store = $data_to_store;
    _import_export__data_store__write($store);
  }
  else {
    return FALSE;
  }
  
  return TRUE;
}


function _import_export__data_store__write($_store = NULL) {
  /*static $store;
  if ($_store) {
    $store = $_store;
  }
  return $store;*/
  if ($_store) {
    cache_set('import_export : current import : data store', $_store);
  }
  return $_store;
}

//function import_export__data_store__exit() { // @@TODO - try to cache this statically
//  $store = _import_export__data_store__write();
//  cache_set('import_export : current import : data store', $store);
//}


