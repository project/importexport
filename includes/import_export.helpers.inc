<?php

/**
 * @@TODO: document
 */
function first_existing() {
  $args = func_get_args();
  do {
    $var = array_shift($args);
    if (isset($var)) {
      return $var;
    }
  } while (count($args) > 0);
  return NULL;
}


// @@RECENT-CHANGE
function import_export_check_for_services_module() {
  if (!module_exists('services')) {
    drupal_set_message('While Import/Export can be used without the Services 3.x module, it is highly recommended that this module be enabled so that imports can be performed from remote servers without the need for downloading exported data first.  Note: You must also enable the REST Server module (included with Services 3.x) if you wish to use this functionality.');
  }
  else if (module_exists('services') && !module_exists('rest_server')) {
    drupal_set_message('In order to perform imports from remote servers without the need for download exported data to your local machine, you must also enable the REST Server module.  This module is included with Services 3.x');
  }
  import_export_check_for_enabled_service();
}

function import_export_check_for_enabled_service() {
  if (function_exists('services_endpoint_load_all')) {
    $endpoints = services_endpoint_load_all();
    foreach ($endpoints as $ep) {
      if ($ep->status == 1) {
        foreach ($ep->resources as $name => $res) {
          if ($name == 'import_export') { // @@TODO: make the resource name a constant
            return TRUE;
          }
        }
      }
    }
    
    drupal_set_message(t('It does not appear that the Import/Export service is enabled.  Please visit the !page to enable it.', array('!page' => l('Services administration page', 'admin/build/services'))));
  }
}