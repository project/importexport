<?php

function import_export_check_node_export_reset_settings() {
  if (variable_get('import_export_stop_all_node_export_resets', FALSE)) {
    return;
  }
  
  $vars = array(
    'node_export_reset_' => t('Publishing options'),
    'node_export_reset_created_' => t('Created date'),
    'node_export_reset_changed_' => t('Changed date'),
    'node_export_reset_menu_' => t('Menu entry'),
    'node_export_reset_path_' => t('Path'),
    'node_export_reset_book_mlid_' => t('Book settings'),
  );
  $types = node_get_types('names');
  $warnings = array_combine(array_keys($types), array_keys($types));
  foreach ($types as $type => $name) {
    foreach ($vars as $var => $human_readable) {
      if (variable_get($var . $type, TRUE)) {
        $warnings[$type][] = $var;
      }
    }
  }
  
  $warning = array();
  foreach ($warnings as $type => $problems) {
    $name = $types[$type];
    $parts = array();
    foreach ($problems as $problem) {
      $parts[] = $vars[$problem];
    }
    $problems = implode(', ', $parts);
    $warning[] = " $name ($problems)";
  }
  
  drupal_set_message(t('Warning!  Nodes of the following content types will have certain options reset when they are imported by the Node Export module: '
                    . '@warnings.  Import Export can prevent Node Export from resetting these values if you check the "Stop Node Export resets" box on the '
                    . '!admin_link before importing this package.',
                        array('@warnings' => implode(', ', $warning),
                              '!admin_link' => l('Import Export admin page', 'admin/content/import-export'))),
                  'warning');
}

function import_export_unset_node_export_reset_settings() {
  $vars = array(
    'node_export_reset_',
    'node_export_reset_created_',
    'node_export_reset_changed_',
    'node_export_reset_menu_',
    'node_export_reset_path_',
    'node_export_reset_book_mlid_',
  );
  $types = node_get_types('names');
  foreach ($types as $type => $name) {
    foreach ($vars as $var) {
      variable_set($var . $type, 0);
    }
  }
}


/**
 * Implementation of hook_node_export_node_alter()
 *
 * Allow the receiving db to record the origin nid for future update deployments.
 */
function import_export_node_export_node_alter(&$node, $original_node, $method) {
  if (in_array($method, array('save-edit', 'prepopulate'))) {
    if (isset($node->override_nid)) { // this node has been set to be updated rather than created
      $node->nid = $node->override_nid; // set the node's nid
    }
  }
  else if ($method == 'export') {
    $node->origin_nid = $original_node->nid;
    $node->origin_vid = $original_node->vid;
  }
}

/**
 * Implementation of hook_node_export_node_bulk_alter()
 */
function import_export_node_export_node_bulk_alter(&$nodes, $op) {
  if (import_export_plugins_are_enabled('node') == FALSE) {
    return;
  }
  
  $origin_namespace = &ctools_static('import_export : current import : origin namespace', '');
  $nodes_to_create = &ctools_static('import_export : current import : nodes to create', array());
  $nodes_to_update = &ctools_static('import_export : current import : nodes to update', array());
  $nodes_copy = (array)$nodes; // make a copy

  if ($op == 'import') {
    // only let node_export import nodes that the user has specified as needing to be created from scratch
    foreach ($nodes_copy as $i => $node) {
      // NODES TO CREATE:
      // disable pathauto, translate site ids
      $node->menu = NULL;
      if (in_array($node->nid, $nodes_to_create)) {
        $node->pathauto_perform_alias = FALSE; // make sure pathauto doesn't attempt to create paths
        $op = 'create';
        drupal_alter('import_export_node', $node, $op, $origin_namespace); // @@TODO: document
        $nodes[$i] = $node;
      }
      // NODES TO UPDATE:
      // disable pathauto, translate site ids, add information to node object about which existing nid to overwrite (because node_export removes nid after this hook)
      else if (in_array($node->nid, $nodes_to_update)) {
        $node->pathauto_perform_alias = FALSE; // make sure pathauto doesn't attempt to create paths
        
        // translate all IDs on node object and update node ourselves
        $node->nid = import_export_translate_id($origin_namespace, import_export_plugin('node')->id_field(), $node->nid);
        $node->vid = import_export_translate_id($origin_namespace, 'vid', $node->vid);
        $uid = import_export_translate_id($origin_namespace, 'uid', $node->uid);
        $revision_uid = import_export_translate_id($origin_namespace, 'uid', $node->revision_uid);
        $node->uid = ($uid ? $uid : $node->uid);
        $node->revision_uid = ($revision_uid ? $revision_uid : $node->revision_uid);
        
        // add on the existing menu item
        //_import_export_add_existing_menu_object_to_node($node);
        
        $op = 'update';
        drupal_alter('import_export_node', $node, $op, $origin_namespace);
        
        // keep this node away from node_export
        unset($nodes[$i]);
        
        if ($node->nid) { // can't update a node with no nid!
          node_save($node);
        }
        else {
          drupal_set_message(t('Could not find node to update for node with origin nid @nid', array('@nid' => $node->origin_nid)), 'error');
        }
      }
      // NODES TO NOT IMPORT AT ALL
      else {
        unset($nodes[$i]);
      }
    }
  }
  // add id translations for new nodes after import is complete
  else if ($op == 'after import') {
    $nodes_to_create = &ctools_static('import_export : current import : nodes to create', array());
    $old_vids = array();
    foreach ($nodes as $node) {
      $origin_nid = NULL;
      $origin_vid = NULL;
      if ($node->origin_nid) {
        $origin_nid = $node->origin_nid;
        $origin_vid = $node->origin_vid;
      }
      else {
        $origin_nid = $node->nid; // @@TODO: explain this comment better: // global pages keep their nids
        $origin_vid = $node->vid;
      }

      // should only add id translation for newly created nodes
      if (in_array($origin_nid, $nodes_to_create)) {
        // record the old nid->new nid / old vid->new vid translations
        import_export_add_id_translation($origin_namespace, import_export_plugin('node')->id_field(), $origin_nid, $node->nid);

        $res = db_query("SELECT vid FROM {node} WHERE nid = %d", $node->nid);
        if ($new_vid = db_result($res)) {
          import_export_add_id_translation($origin_namespace, 'vid', $origin_vid, $new_vid);
        }
        //$old_vids[$node->nid] = $origin_vid;
      }
    }
    
    // @@TODO: test vid translations and then remove this if possible
    // set vid translations (node revisions)
    /*$vids = array();
    $placeholders = implode(',', array_fill(0, count($old_vids), '%d'));
    $res = db_query("SELECT nid, vid FROM {node} WHERE nid IN ($placeholders)", array_keys($old_vids));
    while ($row = db_fetch_object($res)) {
      $vids[$old_vids[$row->nid]] = $row->vid; // $vids[old vid] = new vid
    }
    foreach ($vids as $old_vid => $new_vid) {
      import_export_add_id_translation($origin_namespace, 'vid', $old_vid, $new_vid);
    }*/
  }
}

// @@TODO: this function does not actually end up seeming to be necessary
/*function _import_export_add_existing_menu_object_to_node(&$node) {
  if (!function_exists('menu_nodeapi')) {
    module_load_include('module', 'menu');
  }
  menu_nodeapi($node, 'prepare');
}*/
