Drupal.behaviors.import_export_servers_form = function() {
  // make sure lines with hidden input fields are the same height as others
  var height = $('#new-server-1').css('height');
  $('.existing-server span').each(function() {
    $(this).css('height', height);
  });
  
  $('.edit-link').click(function() {
    var which = $(this).attr('id').substr(('edit-server-').length);
    $('#server-' + which).css('display', 'inline-block'); // show edit box
    $('#li-server-' + which + ' span').css('display', 'none'); // hide links
  });
  
  $('.delete-link').click(function() {
    var which = $(this).attr('id').substr(('delete-server-').length);
    
    // clear the box and move it out of the OL/LI so Drupal FAPI can still see it
    $('#server-' + which).val('').css('display', 'none').appendTo('#import-export-import-servers-form');
    
    // remove the LI
    $('#li-server-' + which).remove();
  });
  
  $('#add-server').click(function() {
    var firstAvailable = 1;
    while ($('#servers #new-server-'+firstAvailable).length > 0) {
      firstAvailable++;
    }
    $('<li><div class="form-item"><input class="new-server" id="new-server-'+firstAvailable+'"/></div></li>').appendTo('#servers');
  });
  
  $('#edit-submit').click(function() {
    var data = [];
    $('#servers input').each(function() {
      data.push($(this).val());
    });
    $(data).each(function(i, val) {
      data[i] = encodeURIComponent(val);
    })
    $('#edit-servers-data').val(data.join('#'));
  });
}