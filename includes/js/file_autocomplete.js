Drupal.behaviors.import_export_file_autocomplete = function() {
  Drupal.settings.import_export.fill_files_autocomplete_results_with_files = function(files) {
    // clear list
    $('#edit-file-files-autocomplete-files option').remove();

    // add new elements
    for (var fid in files) {
      if ($('#edit-selected-files option[value='+fid+']').length <= 0) { // if this isn't already in the selected-files box
        var filename = files[fid];
        $('<option value="'+fid+'">'+filename+' ('+fid+')</option>').appendTo('#edit-file-files-autocomplete-files');
      }
    }
  }
  
  
  // autocomplete box behaviors
  $('#edit-file-files-file-autocomplete').keyup(function() {
    var searchString = $('#edit-file-files-file-autocomplete').val();
    var remainingFiles = {};
    if ($.trim(searchString).length > 0) {
      for (var fid in Drupal.settings.import_export.files) {
        if (Drupal.settings.import_export.files[fid].indexOf(searchString) > -1) {
          remainingFiles[fid] = Drupal.settings.import_export.files[fid];
        }
      }
      // fill results box
      Drupal.settings.import_export.fill_files_autocomplete_results_with_files(remainingFiles);
    }
    else {
      if ($('#edit-file-files-autocomplete-files option').length != Drupal.settings.import_export.files.length) {
        // fill results box
        Drupal.settings.import_export.fill_files_autocomplete_results_with_files(Drupal.settings.import_export.files);
      }
    }
  });

  // add button
  $('#edit-add-files').click(function() {
    $('#edit-file-files-autocomplete-files option:selected').appendTo('#edit-selected-files');
  });
  
  // remove button
  $('#edit-remove-files').click(function() {
    $('#edit-selected-files option:selected').appendTo('#edit-file-files-autocomplete-files');
  });
  
  // submit button
  $('#edit-submit').click(function() {
    var fids = [];
    var i = 0;
    $('#edit-selected-files option').each(function() {
      fids[i++] = $(this).val();
    });
    $('#edit-file-files-selected-files-value').val(fids.join(','));
    return true; // submit the form
  });
  
  // populate autocomplete box
  Drupal.settings.import_export.fill_files_autocomplete_results_with_files(Drupal.settings.import_export.files);
}


