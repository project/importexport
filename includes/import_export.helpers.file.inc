<?php

/**
 * Implementation of hook_import_export_node_alter().
 */
function import_export_import_export_node_alter(&$node, $op, $origin_namespace) {
  if ($op == 'create' || $op == 'update') {
    if (import_export_plugins_are_enabled('file')) {
      // this is on behalf of the files import/export plugin
      $fids_to_replace = array();
      $matches = array();
      if ($count = preg_match_all('/\[ibimage==(?P<fid>\d+)/', $node->body, $matches)) {
        foreach ($matches['fid'] as $origin_fid) {
          if ($dest_fid = import_export_translate_id($origin_namespace, import_export_plugin('file')->id_field(), $origin_fid)) {
            $node->body = str_replace("[ibimage==$origin_fid", "[ibimage==$dest_fid", $node->body);
          }
        }
      }
    }
  }
}