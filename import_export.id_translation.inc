<?php

function import_export_translate_id($origin_namespace, $type, $origin_id) {
  $translations = &ctools_static('import_export : id translations', array());
  if (!$translations) {
    _import_export_import_load_id_translations();
  }
  $translation = ($translations[$origin_namespace][$type][$origin_id] ? $translations[$origin_namespace][$type][$origin_id] : NULL);  
  drupal_alter('import_export_translate_id', $translation, $translations, $origin_namespace, $type, $origin_id);  // @@TODO: document hook
  return $translation;
}

function _import_export_import_load_id_translations() {
  $translations = &ctools_static('import_export : id translations', array());
  $res = db_query("SELECT * FROM {import_export_id_translations}");
  while ($row = db_fetch_object($res)) {
    $translations[$row->origin_namespace][$row->type][$row->origin_id] = $row->destination_id;
  }
  $results = module_invoke_all('import_export_id_translations'); // @@TODO: document hook
  if (is_array($results)) {
    $translations = array_merge_recursive($translations, $results);
  }
  drupal_alter('import_export_id_translations', $translations);  // @@TODO: document hook
  return $translations;
}


function import_export_add_id_translation($origin_namespace, $type, $origin_id, $destination_id) {
  $translations = &ctools_static('import_export : id translations', array());
  $translations[$origin_namespace][$type][$origin_id] = $destination_id;
  $success = db_query("INSERT INTO {import_export_id_translations} (origin_namespace, type, origin_id, destination_id) VALUES ('%s', '%s', '%s', '%s')", $origin_namespace, $type, $origin_id, $destination_id);
  return $success;
}


function import_export_delete_id_translation($type, $destination_id) {
  $success = db_query("DELETE FROM {import_export_id_translations} WHERE type = '%s' AND destination_id = '%s'", $type, $destination_id);
  $translations = &ctools_static('import_export : id translations', array());
  $translations = NULL; // invalidate cache so translations must be reloaded
  return $success;
}