<?php

function import_export__import__do_batch($import_settings) {
  import_export_load_unserialize_dependencies();
  
  $metadata = import_export__data_store__retrieve('metadata');
  $method = $metadata['method'];
  
  // set up batch import operations
  $operations = array();
  $operations[] = array('import_export__import_batch__prepare_batch_context', array($metadata));
  
  foreach ($import_settings as $plugin_key => $object_settings) {
    $operations[] = array('import_export__import_batch__do_operation', array(import_export_get_data_plugin_name_by_key($plugin_key), $method, 'acquire_objects', $import_settings, $metadata));
  }
  
  if ($method == 'server') {
    $operations[] = array('import_export__import_batch__finished_acquiring', array('done')); // @@TODO: remove 'done' if possible and just have an empty array or, better yet, no 2nd parameter
  }
  
  foreach ($import_settings as $plugin_key => $object_settings) {
    $operations[] = array('import_export__import_batch__do_operation', array(import_export_get_data_plugin_name_by_key($plugin_key), $method, 'prepare_objects', $import_settings, $metadata));
    $operations[] = array('import_export__import_batch__do_operation',  array(import_export_get_data_plugin_name_by_key($plugin_key), $method, 'import_objects', $import_settings, $metadata));
  }
  
  $batch = array(
    'operations' => $operations,
    'finished' => 'import_export__import__batch_finished',
    'title' => t('Processing Import'),
    'init_message' => t('Import is starting.'),
    'progress_message' => t('Imported @percentage% out of @total.'),
    'error_message' => t('Import Export has encountered an error during the batch import.'),
    'file' => drupal_get_path('module', 'import_export') . '/import_export.batch.inc',
  );
  batch_set($batch);
}

function import_export__import_batch__finished_acquiring($x, &$batch_context) { // @@TODO: remove $x if possible
  
  import_export__data_store__set_package_not_sparse();
}

function import_export__import_batch__prepare_batch_context($metadata, &$batch_context) {
  import_export_load_unserialize_dependencies();
  $batch_context['message'] = t('Preparing batch import');
  
  // ready the data store (this should already have happened, but you can never be too ready)
  import_export__data_store__ensure_ready();
  
  // @@TODO: set up real object limits
  $global_context['batch object limits'] = array();
  $plugins = array('node', 'file', 'menu', 'site');
  foreach ($plugins as $plugin) {
    $global_context['batch object limits'][$plugin]['acquire_objects']['server'] = 3;
  }
  
  drupal_alter('import_export__global_batch_import_context', $metadata, $global_context);
  cache_set('import_export : current import : batch', $global_context);
  
  $batch_context['finished'] = 1;
}

function __import_export__import_batch__setup_counters(&$batch_context, &$import_settings, $plugin_name, $operation) {
  if (import_export_plugins_are_enabled($plugin_name) == FALSE) {
    return;
  }
  
  $plugin_key = import_export_plugin($plugin_name)->my_key();
  // set up the counter variables
  if (!isset($batch_context['sandbox']['progress'])) {
    $batch_context['sandbox']['total'] = count($import_settings[$plugin_key]);
    $batch_context['sandbox']['limit'] = (int)first_existing($global_context['batch object limits'][$plugin_name][$operation][$method], 0); // a limit exists
    if ($batch_context['sandbox']['limit'] <= 0) {
      $batch_context['sandbox']['limit'] = $batch_context['sandbox']['total'];
    }
    $batch_context['sandbox']['progress'] = 0;
    $batch_context['sandbox']['current_object'] = 0;
  }
  
  // slice the settings array
  $import_settings[$plugin_key] = array_slice($import_settings[$plugin_key], $batch_context['sandbox']['current_object'], $batch_context['sandbox']['limit'], TRUE); // TRUE = preserve keys
}

function import_export__import_batch__do_operation($plugin_name, $method, $operation, $import_settings, $metadata, &$batch_context) {
  if (import_export_plugins_are_enabled($plugin_name) == FALSE) {
    return;
  }
  
  // if there's nothing to acquire, move on
  if (count($import_settings[import_export_plugin($plugin_name)->my_key()]) <= 0) {
    $batch_context['finished'] = 1;
    return;
  }
  
  // get the global context array
  import_export_load_unserialize_dependencies();
  $cache = cache_get('import_export : current import : batch');
  $global_context = $cache->data;
  
  __import_export__import_batch__setup_counters($batch_context, $import_settings, $plugin_name, $operation);
  
  $function = "import_export__import_batch__{$operation}";
  if (function_exists($function)) {
    $function($plugin_name, $method, $import_settings, $metadata, $global_context, $batch_context);
  }
  
  $batch_context['sandbox']['current_object'] += $batch_context['sandbox']['limit'];
  $batch_context['finished'] = (float)($batch_context['sandbox']['current_object'] / $batch_context['sandbox']['total']);
}



function import_export__import_batch__acquire_objects($plugin_name, $method, $import_settings, $metadata, &$global_context, &$batch_context) {
  $success = import_export__data_store__acquire($method, $metadata, $import_settings, $plugin_name); // @@TODO: check $success
  dsm(array('success' => $success, 'import settings' => $import_settings, 'plugin name' => $plugin_name,
        'method' => $method, 'metadata' => $metadata, 'global context' => $global_context, 'batch context' => $batch_context), 'import_export__import_batch__acquire_objects');
  if (!$success) {
    drupal_set_message('import_export__import_batch__acquire_objects(): Could not acquire objects for plugin ' . $plugin_name . " (method: $method, base url: {$metadata['base url']})", 'error');
  }
  $batch_context['message'] = t('Acquiring @plugin data', array('@plugin' => $plugin_name));
}

function import_export__import_batch__prepare_objects($plugin_name, $method, $import_settings, $metadata, &$global_context, &$batch_context) {
  if (import_export_plugins_are_enabled($plugin_name) == FALSE) {
    return;
  }
  
  $package = import_export__data_store__retrieve('package');
  $success = import_export_plugin($plugin_name)->prepare_import_package($package, $method, $import_settings, $metadata);
  import_export__data_store__store('package', $package);
  $batch_context['message'] = t('Preparing @plugin', array('@plugin' => $plugin_name));
}


function import_export__import_batch__import_objects($plugin_name, $method, $import_settings, $metadata, &$global_context, &$batch_context) {
  if (import_export_plugins_are_enabled($plugin_name) == FALSE) {
    return;
  }
  
  $package = import_export__data_store__retrieve('package');
  $success = import_export_plugin($plugin_name)->import($package, $import_settings);
  $batch_context['message'] = t('Importing @plugin', array('@plugin' => $plugin_name));
}



function import_export__import__batch_finished($success, $results, $operations) {
  import_export__data_store__clear();
  if ($success) {
    $message = format_plural(count($results), 'one object imported.', '@count objects imported.');
    drupal_set_message(t('Import performed successfully: @message', array('@message' => $message)));
  }
  else {
    drupal_set_message(t('Import finished but with one or more errors.'));
  }
}
